from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'gistcloud_project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'gistcloud.views.home', name='home'),
    url(r'^login/$', 'gistcloud.views.login_view', name='login'),
    url(r'^logout/$', 'gistcloud.views.logout_view', name='logout'),
    url(r'^register/$', 'gistcloud.views.register', name='register'),
    url(r'^about/$', 'gistcloud.views.about', name='about'),
    url(r'^dashboard/$', 'gistcloud.views.dashboard', name='dashboard'),
    url(r'^dashboard/task_info$', 'gistcloud.views.task_info', name='task_info'),
    url(r'^dashboard/data_sets$', 'gistcloud.views.data_sets', name='data_sets'),
    url(r'^dashboard/results$', 'gistcloud.views.results', name='results'),
    url(r'^dashboard/trial$', 'gistcloud.views.trial', name='trial'),
    url(r'^dashboard/start_trial$', 'gistcloud.views.start_trial', name='start_trial'),
    url(r'^dashboard/get_results', 'gistcloud.views.get_results', name='get_results'),
    url(r'^dashboard/tweet_utils', 'gistcloud.views.tweet_utils', name='tweet_utils'),
)
