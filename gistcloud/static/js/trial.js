/**
 * Created by maria on 4/18/15.
 * ajax POST calls
 * http://coreymaynard.com/blog/performing-ajax-post-requests-in-django/
 *
 */
function startTrial(btn){
    var options;
    var $btn = $(btn);
    var csrftoken = getCookie('csrftoken');
    var classifier = $( "#cls option:selected" ).text();
    var discretization = $( "#discr option:selected" ).text();
    var sel_fea = $( "#fea option:selected");
    var sel_morph = $( "#morph option:selected");
    var sel_fig = $( "#fig option:selected");
    var sel_sim = $( "#sim option:selected");
    var sel_corpus = $( "#corpus option:selected").text();
    var selected_features = [];
    var spinner = '<div class="spinner_container" id="spinner_box"><i class="fa fa-terminal fa-2x">Processing...</i>' +
                    '<i class="fa fa-refresh fa-2x fa-spin spinner"></i></div>';

    $('body').prepend(spinner);

    $.each(sel_fea, function(k, v){
       selected_features.push($(v).text());
    });
    $.each(sel_morph, function(k, v){
       selected_features.push($(v).text());
    });
    $.each(sel_fig, function(k, v){
       selected_features.push($(v).text());
    });
    $.each(sel_sim, function(k, v){
       selected_features.push($(v).text());
    });

    options = {
        "classifier": classifier,
        "discretization": discretization,
        "selected_features": selected_features,
        "corpus": sel_corpus
    };

    var data = JSON.stringify(options);

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });

    $.ajax({url: "../dashboard/start_trial",
        method: "POST",
        async: true,
        data: options,
        "beforeSend": function(xhr, settings) {
            $.ajaxSettings.beforeSend(xhr, settings);
        },
        success: function(result){
            $('#trial_results').html(result);
            $("#spinner_box").remove();
            console.log(trial_results_agree)
            cummulativeLineChart(trial_results_agree, "chart_agree");
            cummulativeLineChart(trial_results_disagree, "chart_disagree");
            polarityBarChart(trial_results_agree.length, trial_results_disagree.length);
    },
    error: function(xhr,status,error){
        showalert(error, "danger");
        $("#spinner_box").remove();
    }
    });

}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


function cummulativeLineChart(data, containerId){
    var y_axis = ['y', -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5];
    var data_len = ['x'];
    var data_agree_initial = ['initial'];
    var data_agree_predicted = ['predicted'];
    data_len[0] = "x";

    for(var i=1; i <= data.length; i++){
        data_len.push(i);
    }

    $.each(data, function(k,v){
        data_agree_initial.push(parseInt(v[1]));
        data_agree_predicted.push(parseInt(v[2]));
    });

    var chart_agree = c3.generate({
        bindto: "#"+containerId,
        data: {
            x: 'x',
    //        xFormat: '%Y%m%d', // 'xFormat' can be used as custom format of 'x'
            columns: [
                data_len,
                data_agree_initial,
                data_agree_predicted
            ]
        },
        type:'area',
        //types: {
        //    initial: 'spline',
        //    predicted: 'area'
        //},
        axis: {
            x: {
                //type: 'area',

                tick: {
                    fit:false
                    //format: '%Y-%m-%d'
                }
            },
            y: {
                max: 5,
                min: -5,
                label: { // ADD
                    text: 'Score',
                    position: 'outer-middle'
                }
            }
        }
    });

}

function polarityBarChart(len_agree, len_disagree){

    var chart = c3.generate({
    data: {
        bindto:'#chart',
        // iris data from R
        columns: [
            ['agree on polarity', len_agree],
            ['disagree on polarity', len_disagree],
        ],
        type : 'pie',
        onclick: function (d, i) { console.log("onclick", d, i); },
        onmouseover: function (d, i) { console.log("onmouseover", d, i); },
        onmouseout: function (d, i) { console.log("onmouseout", d, i); }
    }
});
}