import copy
import json
import datetime
import datetime
from django.core.context_processors import csrf
from django.shortcuts import render, redirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from models import TweetTestData, TweetFinalTestData
from FigurativeTextAnalysis.helpers.globals import g
from FigurativeTextAnalysis.models.Trial import Trial
from django.views.decorators.csrf import ensure_csrf_cookie
from django.contrib.auth.models import User
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login


# Create your views here.

figurative_features = [ 'OH_SO',
                        'DONT_YOU',
                        'AS_GROUND_AS_VEHICLE',
                        ]
morphological_features=['CAPITAL',
                        'HT',
                        'HT_POS',
                        'HT_NEG',
                        'LINK',
                        'POS_SMILEY',
                        'NEG_SMILEY',
                        'NEGATION',
                        'REFERENCE',
                        'questionmark',
                        'exclamation',
                        'fullstop',
                        'RT',
                        'LAUGH',
                        'LOVE']
text_similarity_features = [
                        'res',
                        'lin',
                        'wup',
                        'path', ]
features = ['postags',
           # 'words',
           'swn_score',
           's_word',
           'contains_'
           ]
classifiers = ['NBayes', 'SVM', 'DecisionTree',
                'SVMStandalone', 'SVR', 'NuSVR',
                'RandomForestRegressor', 'SGD', 'SVC']


def home(request):
    return render_to_response('index.html', context_instance=RequestContext(request))


def logout_view(request):
    logout(request)
    return redirect('/')

@ensure_csrf_cookie
def login_view(request):
    c = {}
    c.update(csrf(request))
    if request.method == "POST":
        user = authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
        if user is not None:
            if user.is_active:
                login(request, user)
                c.update({'user': user})
                print("User is valid, active and authenticated")

                return redirect('/dashboard', c)
            else:
                print("The password is valid, but the account has been disabled!")
        else:
            c["Errors"] = "Wrong username or password!"
            return render_to_response('login.html', c, context_instance=RequestContext(request))
    else:
        return render_to_response('login.html', c, context_instance=RequestContext(request))

@ensure_csrf_cookie
def register(request):
    c = {}
    c.update(csrf(request))
    user = None
    if request.method == "POST":
        try:
            username = request.POST.get('username')
            email = request.POST.get('email')
            password = request.POST.get('password')
            if username != '' and email != '' and password != '':
                user = User.objects.create_user(username=username,
                                                email=email,
                                                password=password)
                user.last_name = request.POST.get('last_name')

                user.save()
            else:
                c["Errors"] = "Please complete all required fields!"
        except Exception, ex:
            c["Errors"] = "Username exists!"
        if user is not None:
            return redirect('/login', c)

    return render_to_response('register.html', c, context_instance=RequestContext(request))


def about(request):
    return render_to_response('about.html')

@login_required(login_url='/login')
def dashboard(request):
    context = RequestContext(request)
    data = {'user': request.user, 'csrf_token': context}

    if not request.user.is_authenticated():
        return redirect(request, '/login', context)

    return render_to_response('dashboard.html', data, context_instance=RequestContext(request))

@login_required(login_url='/login')
def task_info(request):
    return render_to_response('task_info.html')

@login_required(login_url='/login')
def tweet_utils(request):
    cleaning_options = {
        "remove_non_ascii":True,
        "remove_rt" : True,
        "remove_laugh": True,
        "split_sentences": True,
        "remove_negations": True,
        "remove_urls": True,
        "remove_emoticons": True,
        "remove_reference": True,
        "remove_special_characters": True,
        "fix_space": True,
        "split_words": True,
        "convert_to_lower": True,
        "remove_multiples": True,
        # self.spellcheck = True
        "remove_stop_words": True
    }
    options = {"cleaning_options": cleaning_options}

    return render_to_response('tweet_utils.html', options)

@login_required(login_url='/login')
def data_sets(request):
    test_data_test = TweetTestData.objects.filter(train=0).count()
    test_data_trial = TweetTestData.objects.filter(train=1).count()
    final_data = TweetFinalTestData.objects.count()
    final = {"DataSets": [{"key": "test set", "val": test_data_test},
                          {"key": "trial set", "val": test_data_trial},
                          {"key": "final trial data", "val": final_data},
                          {"key": "final test data", "val": test_data_test+test_data_trial}]
             }

    return render_to_response('data_sets.html', final)

@login_required(login_url='/login')
@ensure_csrf_cookie
def results(request):
    # todo: duplicate code: move this in a get_options method
    classifiers = ['NBayes', 'SVM', 'DecisionTree',
                   'SVMStandalone', 'SVR', 'NuSVR',
                   'RandomForestRegressor', 'SGD', 'SVC']
    discretization = ['1.0', '0.5', '0.2']
    context = RequestContext(request)
    options = {"classifiers": classifiers,
               "morphological": morphological_features,
               "figurative": figurative_features,
               "similarity": text_similarity_features,
               "features": features,
               "discretization": discretization,
               "corpuses" : ["Test", "Final"],
               "csrf_token": context}

    q = "SELECT * FROM SentiFeed.TrialScores inner join SentiFeed.Trial on SentiFeed.TrialScores.trial_id = Trial.id " \
        "inner join SelectedFeatures on Trial.selected_features_id = SelectedFeatures.id " \
        "where SelectedFeatures.AS_GROUND_AS_VEHICLE=1 limit 10000; "



    # data = g.mysql_conn.execute_query(q)

    return render_to_response('results.html', options)

@login_required(login_url='/login')
@ensure_csrf_cookie
def trial(request):
    discretization = ['1.0', '0.5', '0.2']
    context = RequestContext(request)
    options = {"classifiers": classifiers,
               "morphological": morphological_features,
               "figurative": figurative_features,
               "similarity": text_similarity_features,
               "features": features,
               "discretization": discretization,
               "corpuses" : ["Test", "Final"],
               "csrf_token": context}

    return render_to_response('trial.html', options)

@login_required(login_url='/login')
@ensure_csrf_cookie
def start_trial(request):
    print datetime.datetime.now().time()
    cls = request.POST.get('classifier')
    discretization = request.POST.get('discretization')
    corpus = request.POST.get('corpus')
    selected_features = request.POST.getlist(u'selected_features[]')
    cls_type = transform_cls_to_enum_values(cls)
    dscr_type = transform_discretization_to_enum_values(discretization)

    print cls, dscr_type, selected_features, corpus

    trial = Trial(selected_features, cls_type, g.CLASSIFIER_TYPE.DecisionTree, dscr_type, corpus)
    trial.process()
    trial.save_results()
    test_set = trial.tweet_processor.test_set
    test_initial_scores = trial.y_test
    predicted_scores = trial.predictions
    sklearn_precision_score = trial.tweet_processor.classifier.sklearn_precision_score
    prediction_per_initial_agree = []
    prediction_per_initial_disagree = []

    for i in range(0, len(test_initial_scores)):

        temp = predicted_scores[i]

        # if type(predicted_scores[i]) is not float:
        #     print "before", predicted_scores[i]
        #     temp = round(float(trial.tweet_processor.discrete_labels[trial.tweet_processor.predictions[i]][0] +
        #                            trial.tweet_processor.discrete_labels[trial.tweet_processor.predictions[i]][1])/2.0)

        if test_initial_scores[i] > 0:
            if temp > 0:
                prediction_per_initial_agree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "success"])
            else:
                prediction_per_initial_disagree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "success"])
        elif test_initial_scores[i] == 0:
            if temp == 0:
                prediction_per_initial_agree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "info"])
            else:
                prediction_per_initial_disagree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "info"])
        elif test_initial_scores[i] < 0:

            if temp < 0:
                prediction_per_initial_agree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "danger"])
            else:
                prediction_per_initial_disagree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "danger"])

    context = RequestContext(request)

    result = {
                "CosineResult": trial.cosine_similarity,
                "csrf_token": context,
                "Results": {
                  "agree": prediction_per_initial_agree,
                  "disagree": prediction_per_initial_disagree
                },
                "sklearn_precision_score": sklearn_precision_score
              }

    print "end:", datetime.datetime.now().time()
    return render_to_response('start_trial.html', result)

@login_required
def get_results(request):
    cls = request.POST.get('classifier')
    discretization = request.POST.get('discretization')
    corpus = request.POST.get('corpus')
    selected_features = request.POST.getlist(u'selected_features[]')
    cls_type = transform_cls_to_enum_values(cls)
    dscr_type = transform_discretization_to_enum_values(discretization)

    context = RequestContext(request)
    options = {"classifiers": classifiers,
               "morphological": morphological_features,
               "figurative": figurative_features,
               "similarity": text_similarity_features,
               "features": features,
               "discretization": discretization,
               "corpuses" : ["Test", "Final"],
               "csrf_token": context}


    return render_to_response('results.html', options)


def transform_cls_to_enum_values(cls):
    cls_types = [g.CLASSIFIER_TYPE.reverse_mapping[g.CLASSIFIER_TYPE.NBayes],
                 g.CLASSIFIER_TYPE.reverse_mapping[g.CLASSIFIER_TYPE.SVM],
                 g.CLASSIFIER_TYPE.reverse_mapping[g.CLASSIFIER_TYPE.DecisionTree],
                 g.CLASSIFIER_TYPE.reverse_mapping[g.CLASSIFIER_TYPE.SVMStandalone],
                 g.CLASSIFIER_TYPE.reverse_mapping[g.CLASSIFIER_TYPE.SVR],
                 g.CLASSIFIER_TYPE.reverse_mapping[g.CLASSIFIER_TYPE.NuSVR],
                 g.CLASSIFIER_TYPE.reverse_mapping[g.CLASSIFIER_TYPE.RandomForestRegressor],
                 g.CLASSIFIER_TYPE.reverse_mapping[g.CLASSIFIER_TYPE.SGD],
                 g.CLASSIFIER_TYPE.reverse_mapping[g.CLASSIFIER_TYPE.SVC]]

    for each in cls_types:
        if each == cls:
            return cls_types.index(each)


def transform_discretization_to_enum_values(dscr):

    if dscr == "1.0":
        return g.DISCRETIZATION.ONE
    elif dscr == "0.5":
        return g.DISCRETIZATION.HALF
    elif dscr == "0.2":
        return g.DISCRETIZATION.ZERO_POINT_TWO
    else:  # default is no descretization
        return g.DISCRETIZATION.ONE