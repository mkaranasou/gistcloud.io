import copy
import unittest
from itertools import combinations
from FigurativeTextAnalysis.helpers.globals import g
from FigurativeTextAnalysis.models.Trial import Trial

__author__ = 'maria'

selected_features =[
                    'OH_SO',                # * // <<   +
                    'DONT_YOU',             # * // <<   +
                    'AS_GROUND_AS_VEHICLE', # * // <<    +
                    'CAPITAL',              # * // <<   +
                    'HT',                   # <<   +
                    'HT_POS',               # // <<   +
                    'HT_NEG',               # // <<   +
                    'LINK',               # //
                    'POS_SMILEY',           # * // <<  +
                    'NEG_SMILEY',           # * // <<  +
                    'NEGATION',             # //   << +
                    'REFERENCE',            # * // << +
                    'questionmark',         # * // << +
                    'exclamation',          # * //   << +
                     'fullstop',
                    'polarity',           # *
                    'RT',                 # //
                    'LAUGH',              # //
                    'LOVE',               # * //
                    'postags',              # * // << +
                    'words',              # *
                    'swn_score',
                    's_word',               # * // << +
                    't_similarity',
                    'res',                  # <<
                    'lin',
                    'wup',
                    'path',               # //
                    'contains_'
                    ]


class TrialTests(unittest.TestCase):

    def combinations_test(self):
        """
        This should lead to :29^29 combinations multiplied by 9 classifiers, so it is not viable.
        source: http://stackoverflow.com/questions/464864/python-code-to-pick-out-all-possible-combinations-from-a-list
        :return:
        :rtype:
        """
        g.logger.info(list(combinations(selected_features, len(selected_features))))
        for i in xrange(1, len(selected_features)+1):
            g.logger.info(list(combinations(selected_features, i)))
            print i

    def incrementally_add_features_test(self):
        """
        Incrementally add features
        :return:None
        :rtype:None
        """
        for i in range(1, len(selected_features)+1):
            print i
            trial = Trial(selected_features[:i], g.CLASSIFIER_TYPE.SVMStandalone, g.CLASSIFIER_TYPE.DecisionTree, g.DISCRETIZATION.ONE)
            trial.process()
            trial.save_results()

    def incrementally_add_and_remove_if_score_suffers(self):
        """
        Incrementally add features and if score decreases do not include the last feature in next run
        :return:None
        :rtype:None
        """
        previous_score = 0.0
        remove_indexes = []
        for i in range(0, len(selected_features)):
            selected_features_mod = copy.copy(selected_features[:i])
            for each in remove_indexes:
                selected_features_mod.remove(each)
            print i
            trial = Trial(selected_features_mod, g.CLASSIFIER_TYPE.NBayes, g.CLASSIFIER_TYPE.DecisionTree, g.DISCRETIZATION.ONE)
            trial.process()
            trial.save_results()
            if previous_score > trial.cosine_similarity:
                print previous_score, trial.cosine_similarity, "going to remove {0}".format(selected_features[i])
                remove_indexes.append(selected_features[i])
            else:
                previous_score = trial.cosine_similarity

    def one_by_one_features_test(self):
        """
        Incrementally add features
        :return:None
        :rtype:None
        """
        for i in range(0, len(selected_features)):
            print i
            trial = Trial(selected_features[i], g.CLASSIFIER_TYPE.SVMStandalone, g.CLASSIFIER_TYPE.DecisionTree, g.DISCRETIZATION.ONE)
            trial.process()
            trial.save_results()

    def by_15_features_test(self):
        """
        Combinations by 15
        :return:None
        :rtype:None
        """
        combinations_list = list(combinations(selected_features, 15))
        print len(combinations_list)
        for combination in combinations_list:
            print combination
            trial = Trial(combination, g.CLASSIFIER_TYPE.SVMStandalone, g.CLASSIFIER_TYPE.DecisionTree, g.DISCRETIZATION.ONE)
            trial.process()
            trial.save_results()