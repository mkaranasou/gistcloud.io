from FigurativeTextAnalysis.models.TextTagger import TextTagger
from FigurativeTextAnalysis.helpers.globals import g

__author__ = 'maria'

import unittest


class MyTestCase(unittest.TestCase):

    def test_laugh_pattern(self):
        text = "the amount of schoolwork i have is so absurd i might just watch the WNBA instead. ahahahaha #NOT"
        tags_result ={'OH_SO': False, 'LOVE': False, 'AS_GROUND_AS_VEHICLE': False, 'HT': False, 'HT_POS': False,
                      'HT_NEG': True, 'POS_SMILEY': False, 'LINK': False,
                      'CAPITAL': True, 'DONT_YOU': False, 'RT': False, 'NEG_SMILEY': False, 'REFERENCE': False,  'NEGATION': True,
                      'LAUGH': True}
        tagger = TextTagger(None)
        tagger.tag_text(text)
        print tagger._tags

        self.assertDictEqual(tags_result, tagger._tags)

    def test_OH_SO_pattern(self):
        text = "Oh, so the amount of schoolwork i have is so absurd i might just watch the wnba instead. ahahahaha"
        tags_result ={'OH_SO': True, 'LOVE': False, 'AS_GROUND_AS_VEHICLE': False, 'HT': False, 'HT_POS': False,
                      'HT_NEG': False, 'POS_SMILEY': False, 'LINK': False,
                      'CAPITAL': False, 'DONT_YOU': False, 'RT': False, 'NEG_SMILEY': False, 'REFERENCE': False,
                      'NEGATION': False,
                      'LAUGH': True}
        tagger = TextTagger(None)
        tagger.tag_text(text)
        print tagger._tags

        self.assertDictEqual(tags_result, tagger._tags)

    def test_CAPITALS_pattern(self):
        text = "Oh, so the amount of schoolwork i have is so absurd i might just watch the WNBA instead. #not #boring"
        tags_result ={'OH_SO': True, 'LOVE': False, 'AS_GROUND_AS_VEHICLE': False, 'HT': False, 'HT_POS': False,
                      'HT_NEG': True, 'POS_SMILEY': False, 'LINK': False,
                      'CAPITAL': True, 'DONT_YOU': False, 'RT': False, 'NEG_SMILEY': False, 'REFERENCE': False,
                      'NEGATION': True,
                      'LAUGH': False}
        tagger = TextTagger(None)
        tagger.tag_text(text)
        print tagger._tags

        self.assertDictEqual(tags_result, tagger._tags)

    def test_negation_pattern(self):
        text = "the amount of schoolwork i have is so absurd i might just watch the WNBA instead#NOT"
        tags_result ={'OH_SO': False, 'LOVE': False, 'AS_GROUND_AS_VEHICLE': False, 'HT': False, 'HT_POS': False,
                      'HT_NEG': True, 'POS_SMILEY': False, 'LINK': False,
                      'CAPITAL': True, 'DONT_YOU': False, 'RT': False, 'NEG_SMILEY': False, 'REFERENCE': False,
                      'NEGATION': True,
                      'LAUGH': False}
        tagger = TextTagger(None)
        tagger.tag_text(text)
        print tagger._tags

        self.assertDictEqual(tags_result, tagger._tags)

    def test_ht_neg_pattern2(self):
        text = "the amount of schoolwork i have is so absurd i might just watch the WNBA instead#NOT#fml"
        text2 = "the amount of schoolwork i have is so absurd i might just watch the WNBA instead#NOT"
        tags_result ={'OH_SO': False, 'LOVE': False, 'AS_GROUND_AS_VEHICLE': False, 'HT': False, 'HT_POS': False,
                      'HT_NEG': True, 'POS_SMILEY': False, 'LINK': False,
                      'CAPITAL': True, 'DONT_YOU': False, 'RT': False, 'NEG_SMILEY': False, 'REFERENCE': False,
                      'NEGATION': True,
                      'LAUGH': False}
        tagger = TextTagger(None)
        tagger.tag_text(text)
        tagger.tag_text(text2)
        print tagger._tags

        self.assertDictEqual(tags_result, tagger._tags)

if __name__ == '__main__':
    unittest.main()
