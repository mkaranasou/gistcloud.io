import ast
import re
from FigurativeTextAnalysis.processors.TweetProcessor import TweetProcessor

__author__ = 'maria'
from FigurativeTextAnalysis.helpers.globals import g
import unittest


class MyTestCase(unittest.TestCase):
    def test_something(self):
        q= ''' select id, feature_dict from tweetbo;'''
        data = g.mysql_conn.execute_query(q)
        for each in data:
            text = str(each[1])
            problematic = re.findall(r'(,|:)\s\'\w+\'[a-z]\':', each[1])
            print "problematic:", problematic
            if len(problematic) > 0:
                print each[1]
                for e in problematic:
                    text = text.replace(e, "")
                # print text
                print ast.literal_eval(text)
        # self.assertEqual(True, False)
    def test_preprocess(self):
        selected_features = ['OH_SO', 'DONT_YOU', 'AS_GROUND_AS_VEHICLE', 'CAPITAL', 'HT', 'HT_POS', 'HT_NEG', 'LINK',
                     'POS_SMILEY', 'NEG_SMILEY', 'NEGATION', 'REFERENCE', 'questionmark', 'exclamation', 'fullstop', 'polarity'
                     'RT', 'LAUGH', 'LOVE',  'postags', 'words', 'swn_score', 's_word',
                    't_similarity', 'res', 'lin', 'wup', 'path', 'contains_']

        tweet_processor = TweetProcessor('', '', selected_features,
                                 g.CLASSIFIER_TYPE.DecisionTree, g.CLASSIFIER_TYPE.SVMStandalone, 1.0)
        # tweet_processor.fix_problematic('../data/figurative_lang_data/probelmatic.csv')
        # tweet_processor.get_train_set_from_db()
        tweet_processor.get_test_set_from_db()
        # tweet_processor.get_feature_trainset()
        tweet_processor.get_feature_testset()
        tweet_processor.reprocess(save=False, trial=True)
        # tweet_processor.process_tweet_set(tweet_processor.train_set)
        # tweet_processor.process_tweet_set(tweet_processor.test_set)
        # tweet_processor.classify()
        # tweet_processor.prepare_labels()



if __name__ == '__main__':
    unittest.main()
