import ast
import random
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from FigurativeTextAnalysis.models.Classifier import Classifier
from FigurativeTextAnalysis.models.SemanticAnalyser import SemanticAnalyzer
from FigurativeTextAnalysis.helpers.globals import g
from nltk.corpus import wordnet as wn
from nltk.corpus import wordnet_ic
brown_ic = wordnet_ic.ic('ic-brown.dat')
semcor_ic = wordnet_ic.ic('ic-semcor.dat')

__author__ = 'maria'

import unittest


class ClassifierTests(unittest.TestCase):
    q_text = '''select id, text, pnn from TweetBOOld where train="{0}"'''
    q_other_text = '''select id, TweetText, EmotionT from SentiFeed.TwitterCorpusNoEmoticons limit 5000;'''
    q_other_text_tc = '''SELECT TweetId, TweetText, EmotionT FROM SentiFeed.TwitterCorpusV2 where EmotionT!="irrelevant" limit 5000;'''
    q_feat_dict = '''select id, feature_dict, tags, pnn from tweetbo where train="{0}"'''
    q_feat_dict_old = '''select id, tags, pnn from TweetBOOld where train="{0}"'''
    q_clean_text = '''select id, clean_text, pnn from TweetBOOld where train="{0}"'''
    x_train = []
    y_train = []
    x_test = []
    y_test = []

    def test_helper_classifier_with_text_NB(self):
        train_data = g.mysql_conn.execute_query(self.q_text.format(1))
        test_data = g.mysql_conn.execute_query(self.q_text.format(0))
        match = 0.0

        x_train = [x[1] for x in train_data]
        y_train = [x[2] for x in train_data]
        x_test = [x[1] for x in test_data]
        y_test = [x[2] for x in test_data]
        cls = Classifier(g.CLASSIFIER_TYPE.NBayes, cls_helper_type=g.CLASSIFIER_TYPE.NBayes)
        cls.set_up_data_for_helper(x_train, y_train, x_test)
        cls.train_helper()
        predictions = cls.get_helper_predictions()
        for i in range(0, len(predictions)):
            if predictions[i] == y_test[i]:
                match += 1.0

        print match, len(predictions),  match/float(len(predictions))

    def test_helper_classifier_with_clean_text_NB(self):
        train_data = g.mysql_conn.execute_query(self.q_clean_text.format(1))
        test_data = g.mysql_conn.execute_query(self.q_clean_text.format(0))
        match = 0.0

        x_train = [x[1] for x in train_data]
        y_train = [x[2] for x in train_data]
        x_test = [x[1] for x in test_data]
        y_test = [x[2] for x in test_data]
        cls = Classifier(g.CLASSIFIER_TYPE.NBayes, cls_helper_type=g.CLASSIFIER_TYPE.NBayes)
        cls.set_up_data_for_helper(x_train, y_train, x_test)
        cls.train_helper()
        predictions = cls.get_helper_predictions()
        for i in range(0, len(predictions)):
            if predictions[i] == y_test[i]:
                match += 1.0

        print match, len(predictions),  match/float(len(predictions))

    def test_helper_classifier_with_feature_dict_text_NB(self):
        train_data = g.mysql_conn.execute_query(self.q_feat_dict.format(1))
        test_data = g.mysql_conn.execute_query(self.q_feat_dict.format(0))
        match = 0.0

        x_train = [x[1] for x in train_data]
        y_train = [x[2] for x in train_data]
        x_test = [x[1] for x in test_data]
        y_test = [x[2] for x in test_data]
        cls = Classifier(g.CLASSIFIER_TYPE.NBayes, cls_helper_type=g.CLASSIFIER_TYPE.NBayes)
        cls.set_up_data_for_helper(x_train, y_train, x_test)
        cls.train_helper()
        predictions = cls.get_helper_predictions()
        for i in range(0, len(predictions)):
            if predictions[i] == y_test[i]:
                match += 1.0

        print match, len(predictions),  match/float(len(predictions))

        #self.assertEqual(True, False)

    def test_helper_classifier_with_text_DT(self):
        train_data = g.mysql_conn.execute_query(self.q_text.format(1))
        test_data = g.mysql_conn.execute_query(self.q_text.format(0))
        match = 0.0

        x_train = [x[1] for x in train_data]
        y_train = [x[2] for x in train_data]
        x_test = [x[1] for x in test_data]
        y_test = [x[2] for x in test_data]
        cls = Classifier(g.CLASSIFIER_TYPE.NBayes, cls_helper_type=g.CLASSIFIER_TYPE.DecisionTree)
        cls.set_up_data_for_helper(x_train, y_train, x_test)
        cls.train_helper()
        predictions = cls.get_helper_predictions()
        for i in range(0, len(predictions)):
            if predictions[i] == y_test[i]:
                match += 1.0

        print match, len(predictions),  match/float(len(predictions))

    def test_helper_classifier_with_clean_text_DT(self):
        train_data = g.mysql_conn.execute_query(self.q_clean_text.format(1))
        test_data = g.mysql_conn.execute_query(self.q_clean_text.format(0))
        match = 0.0

        x_train = [x[1] for x in train_data]
        y_train = [x[2] for x in train_data]
        x_test = [x[1] for x in test_data]
        y_test = [x[2] for x in test_data]
        cls = Classifier(g.CLASSIFIER_TYPE.NBayes, cls_helper_type=g.CLASSIFIER_TYPE.DecisionTree)
        cls.set_up_data_for_helper(x_train, y_train, x_test)
        cls.train_helper()
        predictions = cls.get_helper_predictions()
        for i in range(0, len(predictions)):
            if predictions[i] == y_test[i]:
                match += 1.0

        print match, len(predictions),  match/float(len(predictions))

    def test_helper_classifier_with_feature_dict_text_DT(self):
        train_data = g.mysql_conn.execute_query(self.q_feat_dict.format(1))
        test_data = g.mysql_conn.execute_query(self.q_feat_dict.format(0))
        match = 0.0

        x_train = [x[1] for x in train_data]
        y_train = [x[2] for x in train_data]
        x_test = [x[1] for x in test_data]
        y_test = [x[2] for x in test_data]
        cls = Classifier(g.CLASSIFIER_TYPE.NBayes, cls_helper_type=g.CLASSIFIER_TYPE.DecisionTree)
        cls.set_up_data_for_helper(x_train, y_train, x_test)
        cls.train_helper()
        predictions = cls.get_helper_predictions()
        for i in range(0, len(predictions)):
            if predictions[i] == y_test[i]:
                match += 1.0

        print match, len(predictions),  match/float(len(predictions))

    def test_helper_classifier_with_text_SVM(self):
        train_data = g.mysql_conn.execute_query(self.q_text.format(1))
        test_data = g.mysql_conn.execute_query(self.q_text.format(0))
        match = 0.0

        x_train = [x[1] for x in train_data]
        y_train = [x[2] for x in train_data]
        x_test = [x[1] for x in test_data]
        y_test = [x[2] for x in test_data]
        cls = Classifier(g.CLASSIFIER_TYPE.NBayes, cls_helper_type=g.CLASSIFIER_TYPE.SVM)
        cls.set_up_data_for_helper(x_train, y_train, x_test)
        cls.train_helper()
        predictions = cls.get_helper_predictions()
        for i in range(0, len(predictions)):
            if predictions[i] == y_test[i]:
                match += 1.0

        print match, len(predictions),  match/float(len(predictions))

    def test_helper_classifier_with_clean_text_SVM(self):
        train_data = g.mysql_conn.execute_query(self.q_clean_text.format(1))
        test_data = g.mysql_conn.execute_query(self.q_clean_text.format(0))
        match = 0.0

        x_train = [x[1] for x in train_data]
        y_train = [x[2] for x in train_data]
        x_test = [x[1] for x in test_data]
        y_test = [x[2] for x in test_data]
        cls = Classifier(g.CLASSIFIER_TYPE.NBayes, cls_helper_type=g.CLASSIFIER_TYPE.SVM)
        cls.set_up_data_for_helper(x_train, y_train, x_test)
        cls.train_helper()
        predictions = cls.get_helper_predictions()
        for i in range(0, len(predictions)):
            if predictions[i] == y_test[i]:
                match += 1.0

        print match, len(predictions),  match/float(len(predictions))

    def test_helper_classifier_with_feature_dict_text_SVM(self):
        train_data = g.mysql_conn.execute_query(self.q_feat_dict.format(1))
        test_data = g.mysql_conn.execute_query(self.q_feat_dict.format(0))
        match = 0.0

        x_train = [x[1] for x in train_data]
        y_train = [x[2] for x in train_data]
        x_test = [x[1] for x in test_data]
        y_test = [x[2] for x in test_data]
        cls = Classifier(g.CLASSIFIER_TYPE.NBayes, cls_helper_type=g.CLASSIFIER_TYPE.SVM)
        cls.set_up_data_for_helper(x_train, y_train, x_test)
        cls.train_helper()
        predictions = cls.get_helper_predictions()
        for i in range(0, len(predictions)):
            if predictions[i] == y_test[i]:
                match += 1.0

        print match, len(predictions),  match/float(len(predictions))

    def test_others(self):
        dictVectorizer = False
        train_data = g.mysql_conn.execute_query(self.q_feat_dict.format(1))
        test_data = g.mysql_conn.execute_query(self.q_feat_dict.format(0))
        match = 0.0
        cls = Classifier(g.CLASSIFIER_TYPE.DecisionTree, cls_helper_type=g.CLASSIFIER_TYPE.SVMStandalone)
        self.vec = None

        labels_to_float = {
            'positive': 2.0,
            'negative': 0.0,
            'neutral': 1.0
        }

        if dictVectorizer:
            x_train = [dict(ast.literal_eval(x[1]).items()+ ast.literal_eval(x[2]).items()) for x in train_data]
            y_train = [labels_to_float[x[3]] if cls.cls_helper_type == g.CLASSIFIER_TYPE.RandomForestRegressor
            or cls.cls_helper_type == g.CLASSIFIER_TYPE.SVR
            or cls.cls_helper_type == g.CLASSIFIER_TYPE.SVMStandalone
                       else x[3] for x in train_data]
            x_test = [dict(ast.literal_eval(x[1]).items()+ ast.literal_eval(x[2]).items()) for x in test_data]
            y_test = [labels_to_float[x[3]] if cls.cls_helper_type == g.CLASSIFIER_TYPE.RandomForestRegressor
            or cls.cls_helper_type == g.CLASSIFIER_TYPE.SVR
            or cls.cls_helper_type == g.CLASSIFIER_TYPE.SVMStandalone
                      else x[3] for x in test_data]
            self.vec = cls.vec
        else:
            x_train = [x[1]+x[2] for x in train_data]
            y_train = [labels_to_float[x[3]] if cls.cls_helper_type == g.CLASSIFIER_TYPE.RandomForestRegressor
            or cls.cls_helper_type == g.CLASSIFIER_TYPE.SVR
            or cls.cls_helper_type == g.CLASSIFIER_TYPE.SVMStandalone
                       else x[2] for x in train_data]
            x_test = [x[1]+x[2] for x in test_data]
            y_test = [labels_to_float[x[3]] if cls.cls_helper_type == g.CLASSIFIER_TYPE.RandomForestRegressor
            or cls.cls_helper_type == g.CLASSIFIER_TYPE.SVR
            or cls.cls_helper_type == g.CLASSIFIER_TYPE.SVMStandalone
                       else x[3] for x in test_data]
            self.vec = cls.count_vectorizer

        # for each in x_train:
            # del each['AS_GROUND_AS_VEHICLE']
            # del each['DONT_YOU']
            # del each['OH_SO']
            # del each['HT_NEG']
            # del each['HT_POS']
            # del each['HT']
        #     # del each['CAPITAL']
        #     x_train[x_train.index(each)] = str(each)
        #
        # for each in x_test:
            # del each['AS_GROUND_AS_VEHICLE']
            # del each['DONT_YOU']
            # del each['OH_SO']
            # del each['HT_NEG']
            # del each['HT_POS']
            # del each['HT']
        #     # del each['CAPITAL']
        #     x_test[x_test.index(each)] = str(each)


        cls.set_up_data_for_helper(x_train, y_train, x_test, self.vec)
        cls.train_helper()
        print self.vec.get_feature_names()
        predictions = cls.get_helper_predictions()
        for i in range(0, len(predictions)):
            # if (predictions[i] > 0 and y_test[i] > 0) or \
            #         (predictions[i] < 0 and y_test[i] < 0) or \
            #         (predictions[i] == 0 and y_test[i] == 0):
            if predictions[i] == y_test[i]:
                match += 1.0
            else:
                print predictions[i], y_test[i]
                print x_test[i]
        print match, len(predictions),  match/float(len(predictions))
        try:
            print cls.get_most_informant_features(cls.helper_cls, self.vec)
        except:
            pass


    def test_mock_data_generation(self):
        # http://stackoverflow.com/questions/20075335/is-wordnet-path-similarity-commutative
        # is it spelled correctly?
        # 1. try as is

        # 2. try synonyms

        # 3. try semantic similarity
        class_names = ["Person", "Customer", "Latitude", "Dog", "Country", "Car", "Message", "Document"]
        class_attributes = [["Name", "Surname"], ["FirstName"], ["Latitude"], ["Name"], ["Name"], ["Make"], ["Text"], ["Text"]]
        categories = ["Human", "Country", "Object", "Car", "Region"]

        similarity_functions = [wn.path_similarity, wn.wup_similarity, wn.res_similarity, wn.lch_similarity, wn.lin_similarity ]

        g.logger.info("Test for class names: {0}".format(class_names))
        g.logger.info("With categories: {0}".format(categories))
        for func in similarity_functions:
            g.logger.info("using : {0}".format(func.__str__()))
            results =[]

            for cls in class_names:
                cls_res = []
                semantic_sim_per_category = []

                for each in categories:
                    a = wn.synset('{0}.n.01'.format(cls.lower()))
                    b = wn.synset('{0}.n.01'.format(each.lower()))

                    sim = func(a, b, brown_ic)
                    # sim = wn.wup_similarity(a, b)
                    # sim = wn.res_similarity(a, b, brown_ic)
                    # sim = wn.lch_similarity(a, b)
                    # sim = wn.lin_similarity(a, b, semcor_ic)
                    # sim = semantic_analyser.compare(cls, each)
                    # sim = SemanticAnalyzer().example(cls, each, "n", "lin")
                    semantic_sim_per_category.append([cls+" is " + each, sim])
                    cls_res.append(sim)
                max_ = max(cls_res)
                results.append(categories[cls_res.index(max_)])  # saves category name
                g.logger.info("{0} is {1} with score {2}".format(cls, categories[cls_res.index(max_)], cls_res[cls_res.index(max_)]))
            g.logger.info(semantic_sim_per_category)
            names = g.mysql_conn.execute_query("SELECT first_name FROM znextdatageneration.gd_first_names; ")
            countries = g.mysql_conn.execute_query("SELECT country FROM znextdatageneration.gd_countries; ")
            objects = ["mpla mpla", "mpla", "mpla mpla", "mpla", "mpla mpla", "mpla", "mpla mpla", "mpla"]
            cars = ["Mazda", "Audi", "VW", "BMW", "Lamborghini", "Datsun", "Opel", "MG"]
            region = ["0.111", "0.928", "0.52", "3.25", "50.23", "80.3", "90.00", "10.2645"]

            data_sets = [names, countries, objects, cars, region]

            for cls in class_names:
                res = ""
                cls_index = class_names.index(cls)
                res += "\n'" + cls + "':{\n"
                for attr in class_attributes[cls_index]:
                    res += "'{0}':'{1}',\n".format(attr, data_sets[categories.index(results[cls_index])][cls_index])
                res += "\n}"
                g.logger.info(res)
            g.logger.info(g.log_separator)

        # cls = Classifier(g.CLASSIFIER_TYPE.NBayes, cls_helper_type=g.CLASSIFIER_TYPE.DecisionTree)
        # cls.set_x_train([])
        # cls.set_y_train([])
        # cls.train()
        # cls.set_x_trial([])
        #
        # predictions = cls.predict()

    def test_fake_factory(self):
        # https://pypi.python.org/pypi/fake-factory
        from faker import Factory
        fake = Factory.create()
        print fake.name()
        print fake.address()
        print fake.profile()
        print fake.ssn()

    def test_helper_classifier__NB(self):
        train_data =["Name", "FirstName", "FName", "LName", "LastName", "SName"]
        # test_data = g.mysql_conn.execute_query(self.q_text.format(0))
        match = 0.0

        x_train = ["Name", "FirstName", "FName", "LName", "LastName", "SName"]
        y_train = ["HumanName", "HumanName", "HumanName", "HumanSurname", "HumanSurname", "HumanSurname"]
        x_test = ["first_name", "midname"]
        y_test = ["HumanName", "HumanSurname"]
        cls = MultinomialNB()
        vectorizer = CountVectorizer(strip_accents='unicode')
        X = vectorizer.fit_transform(x_train)
        cls.fit(x_test, y_test)
        vectorizer.transform(x_test)
        predictions= cls.predict(x_test)
        for i in range(0, len(predictions)):
            if predictions[i] == y_test[i]:
                match += 1.0

        print match, len(predictions),  match/float(len(predictions))


if __name__ == '__main__':
    unittest.main()