__author__ = 'maria'

from collections import Counter

from streamparse.bolt import Bolt
import json
import time


class FeatureExtractor(Bolt):

    def initialize(self, conf, ctx):
        self.counts = Counter()

    def process(self, tup):
        # Few castings
        null = ""
        true = True
        false = False

        ##Main Logic
        list_words = []
        json_object = json.loads(str(tup.values[0]))
        action_str = "g2_" + str(json_object['action'])
        list_words.append([action_str])

        ##Bot or Not
        if json_object['is_bot']:
            list_words.append(["bot"])
        else:
            list_words.append(["human"])
        ##Anon or LoggedIN
        if json_object['is_anon']:
            list_words.append(["g3_Anon"])
        else:
            list_words.append(["g3_LoggedIN"])

        self.emit_many(list_words)

if __name__ == '__main__':
    FeatureExtractor().run()