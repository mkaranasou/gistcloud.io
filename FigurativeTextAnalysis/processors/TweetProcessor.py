import ast
import copy
import string
import traceback

from FigurativeTextAnalysis.models.Classifier import Classifier
from FigurativeTextAnalysis.models.TweetBO import Tweet
from FigurativeTextAnalysis.helpers.globals import g
from FigurativeTextAnalysis.helpers.scale_converter import ScaleConverter

# Metrics
# http://streamhacker.com/2010/05/17/text-classification-sentiment-analysis-precision-recall/
# http://scikit-learn.org/stable/modules/classes.html#classification-metrics

__author__ = 'maria'

figurative_data_train_file ='../data/figurative_lang_data/newid_weightedtweetdata_retrieved.csv'
figurative_data_test_file = '../data/figurative_lang_data/task-11-trial_data.csv'


class TweetProcessor(object):
    """
    Handles Tweet preprocessing and classification.

    @train_file: csv with train tweets (~8000) as downloaded by script provided by SemEval
    @test_file: csv with test tweets (~1000) as downloaded by script provided by SemEval
    @selected_features: list of string representing the features that will be included in feature dictionaries
                        and will be used for classification
    @classifier_type: enum: the type of classifier to be used (see globals for possible values)
    @helper_classifier: enum: the type of classifier to be used as helper classifier (see globals for possible values)
                        - old implementation -
    @discretization: float: this number is the step with which continuous labels will be partitioned to discrete labels.
                    e.g. labels=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5] with discretization 0.5 the final label range
                    will be: labels=[-5to-4.5, -4.5to-4.0, -4.0to-3.5, .... 3.5to4.0, 4.0to4.5, 4.5to5]
    """
    def __init__(self, train_file, test_file, selected_features, classifier_type, helper_classifier, discretization=0.5,
                 test_or_final_ds="Final"):
        self.discrete_labels = {}
        self.train_file = train_file
        self.test_file = test_file
        self.selected_features = selected_features
        self.classifier_type = classifier_type
        self.test_or_final_ds = test_or_final_ds
        self.classifier = Classifier(cls_type=classifier_type, cls_helper_type=helper_classifier)
        self.train_set = []
        self.test_set = []
        self.feature_list_train = []
        self.score_list_train = []
        self.feature_list_test = []
        self.score_list_test = []
        self.feature_score_dict = {}
        # self.scale_converter = ScaleConverter(old_max=5.0, old_min=-5.0, new_max=2.0, new_min=0.0)
        self.discretization = discretization
        self.discrete_labels = {}
        self.positive_range = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0]
        self.negative_range = [0.0, -1.0, -2.0, -3.0, -4.0, 5.0]
        self.initial_range = [-5.0, -4.0, -3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0]
        self.positive_labels = {}
        self.negative_labels = {}
        self.prepare_labels()
        self.helper_x_list_train = []
        self.helper_y_list_train = []
        self.helper_x_list_test = []
        self.helper_y_list_test = []
        self.pos_list_x_train = []
        self.pos_list_y_train = []
        self.neg_list_x_train = []
        self.neg_list_y_train = []
        self.pos_list_x_test = []
        self.pos_list_y_test = []
        self.neg_list_x_test = []
        self.neg_list_y_test = []
        self.predictions = []
        # g.prepare_gate_pos_tags_model()

    def read_train__set_from_file(self):
        # read test set file into tweets and fill train set list with objects
        with open(self.train_file, 'rb') as trf:
            for each in trf.readlines():
                try:
                    split_line = each.strip('\n').split('\t')
                    tweet = Tweet(split_line[0].encode('utf-8'),
                                  (self.remove_non_ascii_chars(split_line[1])).encode('utf-8'),
                                  initial_score=split_line[3].encode('utf-8'), train=1)
                    self.train_set.append(tweet)
                except UnicodeDecodeError:
                    traceback.print_exc()
                    print "ERROR", each

    def read_test__set_from_file(self):
        # read test set file into tweets and fill train set list with objects
        with open(self.test_file, 'rb') as trf:
            for each in trf.readlines():
                try:
                    split_line = each.strip('\n').split('\t')
                    tweet = Tweet(split_line[0].encode('utf-8'),
                                  (self.remove_non_ascii_chars(split_line[1])).encode('utf-8'),
                                  initial_score=0,
                                  train=0)
                    # self.test_set.append(tweet)
                    self.save(tweet)
                except UnicodeDecodeError:
                    traceback.print_exc()
                    print "ERROR", each

    def get_train_set_from_db(self):
        q = ""
        if self.test_or_final_ds == "Final":
            q = "select id, text, initial_score, feature_dict, train, pos_tagged_text, words " \
                "from gistcloud_db.TweetTestData;"
        else:
            q = "select id, text, initial_score, feature_dict, train, pos_tagged_text, words " \
                "from gistcloud_db.TweetTestData where train=1;"

        train_data = g.mysql_conn.execute_query(q)

        for row in train_data:
            try:
                tweet = Tweet(row[0],                                    # id
                              row[1],                                    # text
                              initial_score=row[2],                      # handcoded score
                              train=row[4],                              # it is a train set
                              feature_dict=ast.literal_eval(row[3]),     # feature dict --> processing has already taken place
                              # pos_tagged_text=ast.literal_eval(row[5]),  # processing has already taken place
                              # words=ast.literal_eval(row[6])
                              # pnn=row[7],                                # positive-negative-neutral ->helper classifier
                              # corrected_words=ast.literal_eval(row[8])
                              )
                self.train_set.append(tweet)
            except:
                print traceback.print_exc()
                g.logger.error("problem with test tweet:\t{0}".format(row[0]))
                pass

    def get_test_set_from_db(self):
        q = ""
        if self.test_or_final_ds == "Final":
            q = "select id, text, initial_score, feature_dict, train, pos_tagged_text, words, pnn " \
                "from gistcloud_db.TweetFinalTestData;"
        else:
            q = "select id, text, initial_score, feature_dict, train, pos_tagged_text, words, pnn " \
                 "from gistcloud_db.TweetTestData where train=0;"

        train_data = g.mysql_conn.execute_query(q)
        print train_data[0]

        for row in train_data:
            try:
                tweet = Tweet(row[0],                                    # id
                              row[1],                                    # text
                              initial_score=row[2],                      # handcoded score
                              feature_dict=ast.literal_eval(row[3]),     # feature dict --> processing has already taken place
                              train=row[4],                              # it is a trial/ test set
                              # pos_tagged_text=ast.literal_eval(row[5]),  # processing has already taken place
                              # words=ast.literal_eval(row[6])
                              # pnn=row[7],                                # positive-negative-neutral ->helper classifier
                              # corrected_words=ast.literal_eval(row[8])
                              )
                self.test_set.append(tweet)
            except:
                g.logger.error("problem with train tweet:\t{0}".format(row[0]))
                pass

    def process_tweet_set(self, list_of_tweets):
        i = 0
        for tweet in list_of_tweets:
            g.logger.debug("#########################################################################################")
            g.logger.debug("{0}\t{1}".format(list_of_tweets.index(tweet), tweet.text))
            tweet.tag()
            tweet.clean()
            tweet.spellcheck()
            # tweet.fix_words()
            tweet.pos_tag()
            tweet.get_swn_score()
            tweet.get_semantic_similarity('res')
            tweet.get_words_to_swn_score_dict()
            tweet.gather_dicts()
            self.save(tweet)
            i += 1
            print(i)

    def excluded(self, k, dicta_k_value):
        if 's_word' in k:                                   # if k is a sentiwordnet feature
            if 's_word' not in self.selected_features:      # if swn is not wanted
                return True                                 # remove
            return False
        if 'contains_' in k:                                # if k is a sentiwordnet feature
            if 'contains_' not in self.selected_features:   # if swn is not wanted
                return True                                 # remove
            return False
        if 'swn_score' in k:                                # if k is sentiwordnet total score
            if 'swn_score' not in self.selected_features:   # if swn is not wanted
                return True                                 # remove
            return False
        if dicta_k_value in g.VERBS or dicta_k_value in g.ADJECTIVES or dicta_k_value in g.ADVERBS or dicta_k_value in g.NOUNS:
            if 'postags' not in self.selected_features:
                return True
            return False
        if 'word-' in dicta_k_value:
            if 'words' not in self.selected_features or 'word' not in self.selected_features:
                return True
            return False
        if 't-similarity' in k:
            if 't_similarity' not in self.selected_features:
                return True
            return False
        if 'res' in k:
            if 'res' not in self.selected_features:
                return True
            return False
        if 'lin' in k:
            if 'res' not in self.selected_features:
                return True
            return False
        if 'path' in k:
            if 'res' not in self.selected_features:
                return True
            return False
        if 'wup' in k:
            if 'res' not in self.selected_features:
                return True
            return False
        if k not in self.selected_features:
            return True
            print "excluded:", k
        return False

    def get_feature_trainset(self):
        for tweet in self.train_set:
            try:
                dicta = copy.deepcopy(tweet.feature_dict)
                for k in dicta.keys():
                    if not self.excluded(k, str(dicta[k])):
                        # if k is not 'polarity':
                        # if dicta[k] == 0:
                        #     dicta[k] = False
                        # elif type(dicta[k]) == int and dicta[k] > 0:
                        #     dicta[k] = True
                        if 's_word-' in k or 'swn_score' in k:
                            dicta[k] = "positive" if dicta[k] > 1.2 else "negative" if dicta[k] < 0.2 else "neutral" \
                                        if 0.95 <= dicta[k] <= 1.05\
                                        else "somewhat_positive" if 1.2 >= dicta[k] > 1.05 \
                                        else "somewhat_negative"
                        if dicta[k] in g.VERBS:
                            dicta[k] = "VB"
                        elif dicta[k] in g.NOUNS:
                            dicta[k] = "NN"
                        elif dicta[k] in g.ADJECTIVES:
                            dicta[k] = "ADJ"
                        elif dicta[k] in g.ADVERBS:
                            dicta[k] = "RB"
                        if 'contains_' in k:
                        #     dicta[k] = 1
                            dicta[k] = "positive" if dicta[k] > 1.2 else "negative" if dicta[k] < 0.2 else "neutral"\
                                        if 0.95 < dicta[k] < 1.05\
                                        else "somewhat_positive" if 1.2 > dicta[k] > 1.05 else "somewhat_negative"
                        # else:
                        #    if dicta[k] == "True":
                        #        dicta[k] = 1
                        #    elif dicta[k] == "False":
                        #        dicta[k] = 0
                        # if 'questionmark' == k or 'exclamation' == k or 'fullstop' == k:
                        #     dicta[k] = "True" if dicta[k] > 0 else "False"
                        # if 'lin' == k or 'res' == k or 'path' == k:
                        #     dicta[k] = "somewhat" if dicta[k] > 0.1 else "not_similar"
                            # dicta[k] = str(round(dicta[k], 1))
                        # if 't_similarity' in k or 'res'==k or 'lin'== k or 'path'==k or 'wup'==k:
                        #     dicta[k] = round(dicta[k], 2)
                    else:
                        del dicta[k]
                # if len(tweet.corrected_words)>1:
                #     dicta['MULTIPLES'] = "True"
                # else:
                #     dicta['MULTIPLES'] = "False"
                self.helper_x_list_train.append(str(dicta))
                self.helper_y_list_train.append(tweet.pnn)
                self.feature_list_train.append(dicta)
                self.score_list_train.append(self.get_modified_label_for(round(tweet.initial_score, 1))
                                             if self.discretization < 1.0
                                             else round(tweet.initial_score, 0) if (self.discretization == 1.0
                                                    and self.classifier_type != g.CLASSIFIER_TYPE.SVR)
                                             else float(round(tweet.initial_score, 0))
                                             if self.classifier_type >= g.CLASSIFIER_TYPE.SVR
                                             else round(tweet.initial_score, 0))
            except Exception:
                g.logger.error("PROBLEM WITH:\t%s" % tweet.id, exc_info=True)

    def get_feature_testset(self):
        for tweet in self.test_set:
            try:
                dicta = copy.deepcopy(tweet.feature_dict)
                for k in dicta.keys():
                    if not self.excluded(k, str(dicta[k])):
                        if 's_word-' in k or 'swn_score' in k:
                            dicta[k] = "positive" if dicta[k] > 1.2 else "negative" if dicta[k] < 0.2 else "neutral" \
                                        if 0.95 <= dicta[k] <= 1.05\
                                        else "somewhat_positive" if 1.2 >= dicta[k] > 1.05 \
                                        else "somewhat_negative"
                        if dicta[k] in g.VERBS:
                            dicta[k] = "VB"
                        elif dicta[k] in g.NOUNS:
                            dicta[k] = "NN"
                        elif dicta[k] in g.ADJECTIVES:
                            dicta[k] = "ADJ"
                        elif dicta[k] in g.ADVERBS:
                            dicta[k] = "RB"
                        if 'contains_' in k:
                            # dicta[k] = 1
                            # dicta[k] = "positive" if dicta[k] > 1.0 else "negative" if dicta[k] < 1.0 else "neutral"
                            dicta[k] = "positive" if dicta[k] > 1.2 else "negative" if dicta[k] < 0.2 else "neutral" \
                                        if 0.95 < dicta[k] < 1.05\
                                        else "somewhat_positive" if 1.2 > dicta[k] > 1.05 else "somewhat_negative"
                        # if 'questionmark' == k or 'exclamation' == k or 'fullstop' == k:
                        #     dicta[k] = "True" if dicta[k] > 0 else "False"
                        # else:
                        #     if dicta[k] == "True":
                        #         dicta[k] = 1
                        #     elif dicta[k] == "False":
                        #         dicta[k] = 0

                        # if 'lin' == k or 'res' == k or 'path' == k:
                        #     dicta[k] = "somewhat" if dicta[k] > 0.1 else "not_similar"
                        # # if k is not 'polarity':
                        # if dicta[k] == 0:
                        #     dicta[k] = False
                        # elif type(dicta[k]) == int and dicta[k] > 0:
                        #     dicta[k] = True
                        # if 's_word-' in k or 'swn_score' in k or 't_similarity' in k or k == 'res':
                        #     dicta[k] = str(round(dicta[k], 1))
                        # if 't_similarity' == k or 'res' == k or 'lin' == k or 'path' == k or 'wup' == k:
                        #     dicta[k] = round(dicta[k], 2)
                    else:
                        del dicta[k]
                # if len(tweet.corrected_words) > 1:
                #     dicta['MULTIPLES'] = "True"
                # else:
                #     dicta['MULTIPLES'] = "False"
                self.helper_x_list_test.append(str(dicta))
                self.helper_y_list_test.append(tweet.pnn)
                self.feature_list_test.append(dicta)
                self.score_list_test.append(self.get_modified_label_for(round(tweet.initial_score, 1))
                                             if self.discretization < 1.0
                                             else round(tweet.initial_score, 0) if (self.discretization == 1.0
                                                    and self.classifier_type != g.CLASSIFIER_TYPE.SVR)
                                             else float(round(tweet.initial_score, 0))
                                             if self.classifier_type >= g.CLASSIFIER_TYPE.SVR
                                             else round(tweet.initial_score, 0))
            except:
                g.logger.error("PROBLEM WITH:\t%s", tweet.id, exc_info=True)

    def prepare_labels(self):
        labels = []
        for each in self.initial_range:                                  # each is 0.0, 1.0 etc
            d = (1.0/self.discretization)
            for i in range(0, int(d)):                                   # 0, 0.0*0.2 == 0.0 || 1.0/0.2 == 5.0
                labels.append(round(self.discretization*i+each, 1))      # 0.0, 0.2*1, 0.2*2 etc
        # print labels

        for i in range(1, len(labels)-1):
            self.discrete_labels['{0}To{1}'.format(labels[i-1], labels[i])] = [labels[i-1], labels[i]]
            # print [labels[i-1], labels[i]]
        self.discrete_labels['zero'] = [0.0, 0.0]
        print "Label range is: ", self.discrete_labels.keys()

    def classify(self):
        self.get_feature_trainset()
        self.get_feature_testset()
        self.classifier.set_y_train(self.score_list_train)
        self.classifier.set_x_train(self.feature_list_train)
        self.classifier.set_x_trial(self.feature_list_test)
        self.classifier.set_y_trial(self.score_list_test)
        self.classifier.train()
        self.predictions = self.classifier.predict()
        match = 0

        for i in range(0, len(self.predictions)):
            if self.discretization == 1.0:
                q = '''UPDATE TweetTestData SET round_score2="{0}" where id = "{1}" AND train=1'''\
                    .format(self.predictions[i],
                             str(self.test_set[i].id))
            else:
                q = '''UPDATE TweetTestData SET round_score2="{0}" where id = "{1}" AND train=1'''\
                    .format((self.discrete_labels[self.predictions[i]][0] +
                             self.discrete_labels[self.predictions[i]][1])/2.0,
                             str(self.test_set[i].id))
            g.mysql_conn.update(q)
            # calculate accuracy
            if self.predictions[i] == self.score_list_test[i]:
                match += 1
            g.logger.debug("id\t{0}\ttext:\t{1}\tpredicted:\t{2}\tactual:\t{3}".format(self.test_set[i].id,
                                                                                        self.test_set[i].text,
                                                                                        self.predictions[i],
                                                                                        self.score_list_test[i]))

        results = "{0}/{1} PERCENTAGE: {2}% \n".format(match,
                                                     len(self.score_list_test),
                                                     (float(match)/float(len(self.score_list_test)))*100)
        g.logger.info(results)
        print results
        try:
            metrics = self.classifier.get_metrics()
            g.logger.info(metrics)
            print metrics
        except:
            pass

    def save(self, tweet):
        qi = g.update_tweet_bo.format(
                                str(tweet.clean_text).replace("\"", "'"),
                                str(tweet.tagged_text).replace("\"", "'"),
                                str(tweet.pos_tagged_text).replace("\"", "'"),
                                str(tweet.tags).replace("\"", "'"),
                                str(tweet.initial_score).replace("\"", "'"),
                                str(tweet.sentences).replace("\"", "'"),
                                str(tweet.words).replace("\"", "'"),
                                str(tweet.links).replace("\"", "'"),
                                str(tweet.corrected_words).replace("\"", "'"),
                                str(tweet.smileys_per_sentence).replace("\"", "'"),
                                str(tweet.uppercase_words_per_sentence).replace("\"", "'"),
                                str(tweet.reference).replace("\"", "'"),
                                str(tweet.hash_list).replace("\"", "'"),
                                str(tweet.non_word_chars_removed).replace("\"", "'"),
                                str(tweet.negations).replace("\"", "'"),
                                str(tweet.swn_score_dict).replace("\"", "'"),
                                tweet.swn_score,
                                str(tweet.feature_dict).replace("\"", "'"),
                                0,
                                int(tweet.train),
                                "True",
                                str(tweet.words_to_swn_score_dict).replace("\"", "'"),
                                tweet.id)
        # q = g.insert_in_tweet_bo.format(tweet.id,
        #                         str(tweet.text).replace("\"", '\\"'),
        #                         str(tweet.initial_score).replace("\"", '\\"'),
        #                         str(tweet.clean_text).replace("\"", '\\"'),
        #                         str(tweet.tagged_text).replace("\"", '\\"'),
        #                         str(tweet.pos_tagged_text).replace("\"", '\\"'),
        #                         str(tweet.tags).replace("\"", '\\"'),
        #                         str(tweet.sentences).replace("\"", '\\"'),
        #                         str(tweet.words).replace("\"", '\\"'),
        #                         str(tweet.links).replace("\"", '\\"'),
        #                         str(tweet.corrected_words).replace("\"", '\\"'),
        #                         str(tweet.smileys_per_sentence).replace("\"", '\\"'),
        #                         str(tweet.uppercase_words_per_sentence).replace("\"", '\\"'),
        #                         str(tweet.reference).replace("\"", '\\"'),
        #                         str(tweet.hash_list).replace("\"", '\\"'),
        #                         str(tweet.non_word_chars_removed).replace("\"", '\\"'),
        #                         str(tweet.negations).replace("\"", '\\"'),
        #                         str(tweet.swn_score_dict).replace("\"", '\\"'),
        #                         tweet.swn_score,
        #                         str(tweet.feature_dict).replace("\"", '\\"'), 0,
        #                         tweet.train)
        # g.logger.debug(q)
        # try:
        #     g.mysql_conn.update(q)
        # except:
        #     g.logger.error("Q:"+q)
        try:
            g.mysql_conn.update(qi)
            g.logger.debug("Qi:"+qi)
        except:
            g.logger.error("error in Qi:"+qi)

    @staticmethod
    def remove_non_ascii_chars(text):
        """
        to solve problem with extra / weird characters when getting data from database
        :return:
        """
        return str(filter(lambda x: x in string.printable, text))

    def fix_problematic(self, problematic_file):
        q = '''select id, text, feature_dict, train from TweetTestData where id = "{0}" '''
        with open(problematic_file, 'r') as problematic:
            for line in problematic.readlines():
                split_line = line.split('\t')
                data = g.mysql_conn.execute_query(q.format(split_line[1].replace("\n", "")))
                for row in data:
                    tweet = Tweet(row[0],                   # id
                                  row[1],                   # text
                                  initial_score=row[2],     # handcoded score
                                  train=1,                  # it is a train set
                                  feature_dict=row[3])      # feature dict --> processing has already taken place
                    self.train_set.append(tweet)
        self.process_tweet_set(self.train_set)

    def get_modified_label_for(self, initial_score):
        res = ""
        for key, value in self.discrete_labels.items():
            if initial_score == 0.0:
                    # res = "initial\t{0}\tcalculated{1}".format(initial_score, 'zero')
                    # g.logger.debug(res)
                    return "zero"
            elif (value[1] < 0.0 or value[0] <= 0.0) and initial_score < 0.0:
                if value[0] <= initial_score < value[1]:
                    # res = "initial\t{0}\tcalculated{1}".format(initial_score, key)
                    # g.logger.debug(res)
                    return str(key)
            elif initial_score > 0.0 and (value[0] <= initial_score <= value[1]):
                    # res = "initial\t{0}\tcalculated{1}".format(initial_score, key)
                    # g.logger.debug(res)
                    return str(key)

        print "did not match ", initial_score

    def prepare_train_set_from_helper_predictions(self, helper_predictions):
        match = 0
        for i in range(0, len(helper_predictions)):
            if helper_predictions[i] == self.helper_y_list_test[i]:
                match += 1
                print "match helper: ", match, len(helper_predictions)
                print len(self.score_list_test)
                # for i in range(0, len(helper_predictions)):
                print helper_predictions[i], "=>", self.score_list_test[i], \
                      helper_predictions[i] == self.score_list_test[i]

                if helper_predictions[i] == "positive":
                    print self.feature_list_test[i]
                    print self.score_list_test[i]
                    self.pos_list_x_test.append(self.feature_list_test[i])
                    self.pos_list_y_test.append(self.score_list_test[i])
                elif helper_predictions[i] == "negative":
                    self.neg_list_x_test.append(self.feature_list_test[i])
                    self.neg_list_y_test.append(self.score_list_test[i])

        print len(self.pos_list_x_test)
        print len(self.neg_list_x_test)

    def reprocess(self, save=False, trial=True):
        """

        :param save:
        :type save:
        :param trial:
        :type trial:
        :return:
        :rtype:
        """
        q = '''update SentiFeed.TweetFinalTestData set feature_dict = "{0}" where id="{1}";'''
        q_text = '''select id, text, pnn from TweetTestData where train="{0}"'''
        q_feat_dict = '''select id, feature_dict, pnn from TweetFinalTestData where train="{0}"'''
        q_clean_text = '''select id, clean_text, pnn from TweetTestData where train="{0}"'''

        if trial:
            train_data = g.mysql_conn.execute_query(q_feat_dict.format(1))
            test_data = g.mysql_conn.execute_query(q_feat_dict.format(0))

            x_train = [x[1] for x in train_data]
            y_train = [x[2] for x in train_data]
            x_test = [x[1] for x in test_data]
            y_test = [x[2] for x in test_data]

            self.classifier.set_up_data_for_helper(x_train,
                                                   y_train,
                                                   x_test, self.classifier.tfidf_vectorizer)

        # else:
            # self.classifier.set_up_data_for_helper(self.helper_x_list_train,
            #                                        self.helper_y_list_train,
            #                                        self.helper_x_list_test, self.classifier.tfidf_vectorizer)
        # self.classifier.train_helper()
        # helper_predictions = self.classifier.get_helper_predictions()
        # match = 0.0
        # for i in range(0, len(helper_predictions)):
        #     if trial:
        #         g.logger.debug("predicted\t{0}\tactual\t{1}".format(helper_predictions[i], y_test[i]))
        #         if helper_predictions[i] == y_test[i]:
        #             match += 1.0
        #     else:
        #         g.logger.debug("predicted\t{0}\tactual\t{1}".format(helper_predictions[i], self.helper_y_list_test[i]))
        #         if helper_predictions[i] == self.helper_y_list_test[i]:
        #             match += 1.0

            # elif helper_predictions[i] == "negative" and self.helper_y_list_test[i] < 0:
            #     match += 1.0
            # elif helper_predictions[i] == "neutral" and self.helper_y_list_test[i] == 0:
            #     match += 1.0
        # print match, float(match/float(len(helper_predictions))), len(helper_predictions)

        if save and not trial:
            for tweet in self.train_set:
                tweet.process_features()
                try:
                    g.mysql_conn.update(q.format(str(tweet.feature_dict), tweet.id))
                except:
                    g.logger.error(q.format(str(tweet.feature_dict), tweet.id))
                    pass
            for tweet in self.test_set:
                # tweet.process_features(helper_predictions[self.test_set.index(tweet)])
                tweet.process_features(None)
                # tweet.feature_dict['polarity'] = helper_predictions[self.test_set.index(tweet)]
                # tweet.process_features(helper_predictions[self.test_set.index(tweet)])
                # print tweet.id
                try:
                    g.mysql_conn.update(q.format(str(tweet.feature_dict), tweet.id))
                except:
                    g.logger.error(q.format(str(tweet.feature_dict), tweet.id))
                    pass