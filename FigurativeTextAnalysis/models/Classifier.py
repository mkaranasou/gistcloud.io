import ast
import copy
import nltk
from numpy.ma import transpose
import scipy
from scipy.spatial.distance import pdist
import sklearn
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.metrics.pairwise import linear_kernel
from sklearn.multiclass import OneVsRestClassifier
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.svm import NuSVC
from sklearn.tree import DecisionTreeClassifier
from FigurativeTextAnalysis.helpers.globals import g
from sklearn.ensemble import RandomForestRegressor
from nltk.metrics import precision, recall, f_measure
from sklearn.metrics import classification_report
# pairwise cosine= http://stackoverflow.com/questions/12118720/python-tf-idf-cosine-to-find-document-similarity

__author__ = 'maria'


class Classifier(object):
    """
    Wrapper class for scikit-learn classifiers.
    """
    def __init__(self, cls_type, cls_helper_type=g.CLASSIFIER_TYPE.DecisionTree):
        self.estimator = sklearn.svm.LinearSVC()
        self.cls_type = cls_type
        self.cls_helper_type = cls_helper_type
        self.cls = self._set_up_classifier(cls_type)
        self.vec = DictVectorizer(sparse=True)
        self.Xtrain = None
        self.Y = None
        self.Xtrial = None
        self.YTrial = None
        self.predictions = []
        self.XtrainCopy = None
        self.tfidf_vectorizer = TfidfVectorizer(use_idf=False, stop_words='english')
        self.helper_cls = self._set_up_classifier(cls_helper_type)
        self.dict_vectorizer = DictVectorizer(sparse=False)
        self.tfidf_transformer = TfidfTransformer(use_idf=False)
        self.count_vectorizer = CountVectorizer(analyzer='word', input='content', lowercase=True, min_df=1, binary=False,
                                                stop_words='english')
        self.sklearn_precision_score = ""

    # HELPER CLASSIFIER ###############################################################################
    def _set_up_classifier(self, cls_type):
        if cls_type == g.CLASSIFIER_TYPE.NBayes:
            return MultinomialNB()
        elif cls_type == g.CLASSIFIER_TYPE.SVM:
            return OneVsRestClassifier(estimator=sklearn.svm.LinearSVC())
        elif cls_type == g.CLASSIFIER_TYPE.DecisionTree:
            return DecisionTreeClassifier()
        elif cls_type == g.CLASSIFIER_TYPE.SVMStandalone:
            return sklearn.svm.LinearSVC(C=1)
        elif cls_type == g.CLASSIFIER_TYPE.SVC:
            return sklearn.svm.SVC(C=1, gamma=0.001)
        elif cls_type == g.CLASSIFIER_TYPE.SVR:
            return sklearn.svm.SVR(kernel='linear', max_iter=150)  # >> 150
        elif cls_type == g.CLASSIFIER_TYPE.NuSVR:
            return sklearn.svm.NuSVR(kernel='linear', max_iter=10)
        elif cls_type == g.CLASSIFIER_TYPE.RandomForestRegressor:
            return RandomForestRegressor(n_estimators=2, max_depth=50)
        elif cls_type == g.CLASSIFIER_TYPE.SGD:
            return SGDClassifier(loss='perceptron', n_iter=25)
        else:
            raise NotImplementedError

    def set_x_train(self, train_list_of_dicts):
        self.Xtrain = self.vec.fit_transform(train_list_of_dicts).toarray()
        g.logger.debug(train_list_of_dicts[0])
        g.logger.debug(self.Xtrain[0:1])
        self.Xtrain = self.tfidf_transformer.transform(self.Xtrain).toarray()
        # self.Xtrain = self.tfidf_transformer.transform(self.Xtrain)
        # self.XtrainCopy = copy.copy(self.Xtrain)
        # self.Xtrain = sklearn.metrics.pairwise.pairwise_distances(self.XtrainCopy[0:1], self.XtrainCopy, metric='cosine', n_jobs=1)
        # self.Xtrain = linear_kernel(self.Xtrain[0:1], self.Xtrain)
        g.logger.debug("feature_names: %s" % self.vec.feature_names_)
        g.logger.debug("vocabulary_: %s" % self.vec.vocabulary_)
        g.logger.debug("Xtrain: %s" % self.Xtrain)

    def set_y_train(self, handcoded_score_list):
        self.Y = handcoded_score_list
        g.logger.debug("Y:: %s" % self.Y)

    def train(self):
        print self.Xtrain.shape[0], len(self.Y)
        # self.cls.fit(transpose(self.Xtrain), self.Y)      # transpose is used when cosine similarities are calculated
        self.cls.fit(self.Xtrain, self.Y)

    def set_x_trial(self, trial_list_of_dicts):
        self.Xtrial = self.vec.transform(trial_list_of_dicts).toarray()
        self.Xtrial = self.tfidf_transformer.transform(self.Xtrial).toarray()
        # self.Xtrial = linear_kernel(self.Xtrial[0:1], self.Xtrial)
        # self.Xtrial = sklearn.metrics.pairwise.pairwise_distances(self.XtrainCopy[0:1], self.Xtrial, metric='cosine', n_jobs=1)
        # x = pdist(transpose(self.Xtrial.toarray()), 'cosine')

    def set_y_trial(self, trial_scores):
        self.YTrial = trial_scores

    def predict(self):
        # self.predictions = self.cls.predict(transpose(self.Xtrial)).tolist()
        self.predictions = self.cls.predict(self.Xtrial).tolist()
        print classification_report(self.YTrial, self.predictions)
        return self.predictions

    def get_metrics(self, vec):
        self.get_most_informant_features(self.cls, vec)
        self.sklearn_precision_score = sklearn.metrics.precision_score(self.YTrial,
                                                                       self.predictions,
                                                                       labels=self.YTrial,
                                                                       average='weighted')
        print self.sklearn_precision_score
        print "precision:", nltk.metrics.precision(set(self.YTrial), set(self.predictions))
        print "recall:", nltk.metrics.recall(set(self.YTrial), set(self.predictions))
        print "f_measure:", nltk.metrics.f_measure(set(self.YTrial), set(self.predictions))

    def get_most_informant_features(self, classifier, vec, n=10):
        print "IMPORTANT FEATURES:\n"

        feature_names = vec.get_feature_names()
        if self.cls_type != g.CLASSIFIER_TYPE.DecisionTree and self.cls_type != g.CLASSIFIER_TYPE.RandomForestRegressor\
                and self.cls_type != g.CLASSIFIER_TYPE.SVR:
            coefs_with_fns = sorted(zip(classifier.coef_[0], feature_names))
            top = zip(coefs_with_fns[:n], coefs_with_fns[:-(n + 1):-1])

            for (coef_1, fn_1), (coef_2, fn_2) in top:
                important_feature = "\t%.4f\t%-15s\n\t%.4f\t%-15s" % (coef_1, fn_1, coef_2, fn_2)
                print important_feature
                g.logger.debug("important_feature %s" % important_feature)

    def test_figurative(self):
        q1 = '''select feature_dict, initial_score from TweetBO where train =1;'''
        q2 = '''select feature_dict, initial_score from TweetBO where train =0;'''
        x_train = []
        y_train = []
        x_test = []
        y_test = []
        train_data = g.mysql_conn.execute_query(q1)
        test_data = g.mysql_conn.execute_query(q2)
        for row in train_data:
            x_train.append(row[0])
            y_train.append('positive' if row[1]>0 else 'negative' if row[1]<0 else 'neutral')

        for row in test_data:
            x_test.append(row[0])
            y_test.append('positive' if row[1]>0 else 'negative' if row[1]<0 else 'neutral')

        # cls = DecisionTreeClassifier()
        # vec = CountVectorizer(analyzer='word',input='content', lowercase=True, min_df=1, binary=False)
        # trans = TfidfVectorizer(use_idf=False, stop_words='english')

        xx_train = self.count_vectorizer.fit_transform(x_train).toarray()
        # xx_train = trans.fit_transform(x_train).toarray()

        xx_test = self.count_vectorizer.transform(x_test).toarray()
        # xx_test = trans.transform(x_test).toarray()

        self.helper_cls.fit(xx_train, y_train)
        predictions = self.helper_cls.predict(xx_test).tolist()
        match = 0
        true_match = 0
        i = 0
        for prediction in predictions:
            print "PREDICTED:", prediction, "ACTUAL", y_test[i]
            # if prediction>0 and y_test[i] > 0:
            #     match+=1
            # if prediction<0 and y_test[i] < 0:
            #     match+=1
            # if prediction==0 and y_test[i] == 0:
            #     match+=1
            if prediction == y_test[i]:
                true_match+=1
            i+=1
        print "match: ", match, predictions.__len__()
        print "true_match: ", true_match
