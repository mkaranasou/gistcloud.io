import re
from nltk.corpus import wordnet
# from FigurativeTextAnalysis.helpers.pyenchant_spell_checker import EnchantSpellChecker
from FigurativeTextAnalysis.models.POSTagger import POSTagger
from FigurativeTextAnalysis.models.SemanticAnalyser import SemanticAnalyzer
from FigurativeTextAnalysis.models.TextCleaner import TextCleaner
from FigurativeTextAnalysis.models.TextTagger import TextTagger
from FigurativeTextAnalysis.helpers.globals import g

__author__ = 'maria'

class Tweet(object):
    def __init__(self, id_, text, train, clean_text="", tagged_text="", pos_tagged_text="", tags="", initial_score="",
                 feature_dict={}, pnn="", words=[], corrected_words=[]):
        self.id = id_
        self.text = text
        self.clean_text = clean_text
        self.tagged_text = tagged_text if tagged_text!="" else ""
        self.pos_tagged_text = pos_tagged_text if (pos_tagged_text != "" or pos_tagged_text!={}) else {}
        self.tags = tags if tags!={} or tags!="" else {}
        self.initial_score = initial_score
        # self.spellchecker = EnchantSpellChecker()
        # self.spellchecker.dict_exists('en')
        self.sentences = []
        self.words = words if words != [] else []
        self.links = []
        self.corrected_words = corrected_words
        self.smileys_per_sentence = []
        self.uppercase_words_per_sentence = []
        self.reference = []
        self.hash_list = []
        self.non_word_chars_removed = []
        self.negations = []
        self.swn_score_dict = {}
        self.swn_score = 0.0
        self.similarity = 0.0
        self.feature_dict = feature_dict if feature_dict != {} else {}
        self.pnn = pnn
        self.words_dict = {}
        self.words_to_swn_score_dict = {}
        self.train = train
        ######## Helpers #########
        self.tagger = TextTagger()
        self.pos_tagger = POSTagger()
        self.semantic_analyser = SemanticAnalyzer()

    def tag(self):
        self.tagged_text = self.tagger.tag_text(self.text)
        self.tags = self.tagger._tags

    def clean(self):
        # get stopwords from db
        stopwords = g.mysql_conn.execute_query(g.select_from_stop_words())
        # init text cleaner
        cleaner = TextCleaner(self, stopwords)
        clean_words = cleaner.clean_tweet()
        self.clean_text = ' '.join(clean_words)

    def spellcheck(self):
        pass
        # for word in self.words:
        #     if self.contains_errors(word):
        #         corrected = self.spellchecker.correct_word(word)
        #         self.words[self.words.index(word)] = corrected[0].replace("'", "\\'") if len(corrected) > 0 else word

    def pos_tag(self):
        self.pos_tagged_text = self.pos_tagger.pos_stem_lematize(self)
        g.logger.debug("POS-TAGS: %s" % self.pos_tagged_text)

    def get_simple_pos_for_swn(self, word):
        if self.pos_tagged_text[word] in g.NOUNS:
            return ' and Category = "n" '
        elif self.pos_tagged_text[word] in g.VERBS:
            return ' and Category = "v" '
        elif self.pos_tagged_text[word] in g.ADVERBS:
            return ' and Category = "r" '
        elif self.pos_tagged_text[word] in g.ADJECTIVES:
            return ' and Category = "a" '
        return ""

    def fix_words(self):
        for word in self.words:
            self.words[self.words.index(word)] = self.pos_tagger.lancaster_stemmer.stem(word)

    def fix_pos_tagging(self):
        for each in self.pos_tagged_text.keys():
            temp = self.pos_tagged_text[each]
            stemmed = self.pos_tagger.lancaster_stemmer.stem(each)
            del self.pos_tagged_text[each]
            self.pos_tagged_text[stemmed] = temp

    def get_swn_score(self):
        '''
        UPDATE `sentifeed`.`sentiwordnet`
        SET
        `ConvertedScale` = (SentimentAssesment*5)/2 - 5
        '''

        final = 0.0
        excluded = 0
        for word in self.words:
            if len(word) > 1:
                try:
                    # first try get equals score with category
                    score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_equals()
                                                        .format(word, self.get_simple_pos_for_swn(word)))
                except:
                    g.logger.error(g.sum_query_figurative_scale_equals().format(word, self.get_simple_pos_for_swn(word)))
                    # if that failed, try get equals without category
                    score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_equals().format(word, ""))
                    g.logger.debug("tried equals for {0} and got {1}".format(word, score[0][0]))

                if score[0][0] > 2.0:
                    # if that failed, try get like with category
                    stemmed_word = self.pos_tagger.lancaster_stemmer.stem(word)
                    try:
                        score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_like
                                                           .format(stemmed_word, self.get_simple_pos_for_swn(word)))
                        g.logger.debug("tried like for {0} and category got {1}".format(stemmed_word, score[0][0]))
                    except:
                        if score[0][0] > 2.0:
                            # if that failed also, try get like without category
                            score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_like
                                                               .format(stemmed_word, ""))
                            g.logger.debug("tried like for {0} and got {1}".format(stemmed_word, score[0][0]))
                if score[0][0] > 2.0:
                            # if that failed also, try get like without category
                            score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_like
                                                               .format(stemmed_word, ""))
                            g.logger.debug("tried like for {0} and got {1}".format(stemmed_word, score[0][0]))
                if score[0][0] is not None and score[0][0] <= 2.0:
                    self.swn_score_dict['s_word-'+str(self.words.index(word))] = round(score[0][0], 2)
                    final += score[0][0]
                else:
                    self.swn_score_dict['s_word-'+str(self.words.index(word))] = 1
                    excluded += 1
            else:
                excluded += 1
        if self.words.__len__() > excluded and self.words > 0:
            self.swn_score = round(final/float(self.words.__len__()-excluded), 2)
        else:
            if len(self.words) > 0:
                self.swn_score = round(final/float(self.words.__len__()), 2)
            else:
                self.swn_score = 1.0  # neutral

        if self.tags[g.TAGS.reverse_mapping[g.TAGS.NEGATION]] or\
        self.tags[g.TAGS.reverse_mapping[g.TAGS.AS_GROUND_AS_VEHICLE]]:
            self.swn_score = self.swn_score - 1.0 if self.swn_score > 1 else self.swn_score * 0.5
        #
        # if self.tags[g.TAGS.reverse_mapping[g.TAGS.POS_SMILEY]] or\
        # self.tags[g.TAGS.reverse_mapping[g.TAGS.HT_POS]]:
        #     self.swn_score = self.swn_score + 1.0 if self.swn_score < 1 else self.swn_score

    def get_semantic_similarity(self, type_):
        similarity = 0.0
        nouns = []
        verbs = []
        adj = []
        adv = []
        for each in self.pos_tagged_text.items():
            if each[1] in g.NOUNS:
                nouns.append((each[0], wordnet.NOUN))
            elif each[1] in g.VERBS:
                verbs.append((each[0], wordnet.VERB))
            elif each[1] in g.ADJECTIVES:
                adj.append((each[0], wordnet.ADJ))
            elif each[1] in g.ADVERBS:
                adv.append((each[0], wordnet.ADV))

        final_list = [nouns, verbs, adj, adv]
        length = 0.0
        for each in final_list:
            if len(each) > 1:
                for i in range(1, len(each)):
                    temp_similarity = self.semantic_analyser.example(each[i-1][0],
                                                                        each[i][0],
                                                                        each[i][1], type_)
                    if temp_similarity is not None:
                        similarity += temp_similarity
                        length += 1.0

        if length > 0:
            g.logger.debug("length %s" % length)
            self.similarity = float(similarity/length)
        else:
            g.logger.debug("length 0 similarity %s" % similarity)
            self.similarity = 1.0
        return self.similarity

    def get_words_to_swn_score_dict(self):
        for word in self.words:
            lemmatized_word = self.pos_tagger.lemmatizer.lemmatize(word, self.pos_tagged_text[word])
            self.words_to_swn_score_dict[lemmatized_word] = self.swn_score_dict['s_word-'+str(self.words.index(word))]

    def get_words_dict(self):
        for word in self.words:
            self.words_dict['word-'+str(self.words.index(word))] = word
        return self.words_dict

    def gather_dicts(self):
        self.feature_dict = dict(self.swn_score_dict.items() +
                                 self.tags.items() +
                                 self.pos_tagged_text.items() +
                                 self.get_words_dict().items() +
                                 self.words_to_swn_score_dict.items())
        self.feature_dict["t-similarity"] = self.similarity
        self.feature_dict["swn_score"] = self.swn_score

    def contains_errors(self, word):
        return self.spellchecker.spell_checker_for_word(word) is not None

    def process_features(self, predicted_score=None):
        for key, value in self.feature_dict.iteritems():
            if "'" in key:
                _value = value
                del self.feature_dict[key]
                self.feature_dict[key.replace("'", "\s")] = _value
            if type(value) == str and "'" in value:
                self.feature_dict[key] = value.replace("'", "\s")
            # if key == "t-similarity":
            #     g.logger.debug("before t-similarity:\t{0}".format(self.feature_dict[key]))
            #     self.feature_dict[key] = self.get_semantic_similarity()
            #     g.logger.debug("after t-similarity:\t{0}".format(self.feature_dict[key]))
            # if key == "NEGATION":
            #     g.logger.debug("before negation:\t{0}".format(self.feature_dict[key]))
            #     self.feature_dict[key] = len(re.findall(g.NEGATIONS_PATTERN, str(self.text).lower()))
            #     g.logger.debug("after negation:\t{0}".format(self.feature_dict[key]))
            # elif key == "CAPITAL":
            #     g.logger.debug("before CAPITAL:\t{0}".format(self.feature_dict[key]))
            #     self.feature_dict[key] = len(re.findall(g.CAPITALS_PATTERN, self.text))
            #     g.logger.debug("after CAPITAL:\t{0}".format(self.feature_dict[key]))
            # elif key == "NEG_SMILEY":
            #     g.logger.debug("before NEG_SMILEY:\t{0}".format(self.feature_dict[key]))
            #     self.feature_dict[key] = len(re.findall(g.NEG_SMILEY_PATTERN, self.text))
            #     g.logger.debug("after NEG_SMILEY:\t{0}".format(self.feature_dict[key]))
            # elif key == "POS_SMILEY":
            #     g.logger.debug("before POS_SMILEY:\t{0}".format(self.feature_dict[key]))
            #     self.feature_dict[key] = len(re.findall(g.POS_SMILEY_PATTERN, self.text))
            #     g.logger.debug("after POS_SMILEY:\t{0}".format(self.feature_dict[key]))
            # elif key == "OH_SO":
            #     g.logger.debug("before OH_SO_PATTERN:\t{0}".format(self.feature_dict[key]))
            #     self.feature_dict[key] = 1 if len(re.findall(g.OH_SO_PATTERN, str(self.text).lower())) > 0 else 0
            #     g.logger.debug("after OH_SO_PATTERN:\t{0}".format(self.feature_dict[key]))
        # if self.feature_dict.has_key('t-similarity'):
        #         g.logger.debug("before t-similarity:\t{0}".format(self.feature_dict[key]))
        #         self.feature_dict[key] = self.get_semantic_similarity()
        #         g.logger.debug("after t-similarity:\t{0}".format(self.feature_dict[key]))
        if predicted_score is None:
            self.feature_dict['polarity'] = "positive" if self.initial_score > 0.0 \
                                           else "negative" if self.initial_score < 0.0\
                                            else "neutral"
        else:
            self.feature_dict['polarity'] = predicted_score
        g.logger.debug("polarity:\t{0}\t{1}".format(self.feature_dict['polarity'], predicted_score))

        self.feature_dict["questionmark"] = len(re.findall("\?", self.text))
        self.feature_dict["fullstop"] = len(re.findall("\.", self.text))
        self.feature_dict["exclamation"] = len(re.findall("!", self.text))
        self.feature_dict['res'] = self.get_semantic_similarity('res')
        self.feature_dict['lin'] = self.get_semantic_similarity('lin')
        self.feature_dict['path'] = self.get_semantic_similarity('path')
        self.feature_dict['wup'] = self.get_semantic_similarity('wup')
