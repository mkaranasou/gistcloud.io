import ast

__author__ = 'maria'

from FigurativeTextAnalysis.helpers.globals import g

class SelectedFeatures(object):
    _selected_features = {
        'OH_SO': 0,
        'DONT_YOU': 0,
        'AS_GROUND_AS_VEHICLE': 0,
        'CAPITAL': 0,
        'HT': 0,
        'HT_POS': 0,
        'HT_NEG': 0,
        'LINK': 0,
        'POS_SMILEY': 0,
        'NEG_SMILEY': 0,
        'NEGATION': 0,
        'REFERENCE': 0,
        'questionmark': 0,
        'fullstop': 0,
        'fullstop': 0,
        'polarity': 0,
        'RT': 0,
        'LAUGH': 0,
        'LOVE': 0,
        'postags': 0,
        'words': 0,
        'swn_score': 0,
        's_word': 0,
        't_similarity': 0,
        'res': 0,
        'lin': 0,
        'wup': 0,
        'path': 0
    }
    selected_features_init = ['OH_SO', 'DONT_YOU', 'AS_GROUND_AS_VEHICLE', 'CAPITAL', 'HT', 'HT_POS', 'HT_NEG', 'LINK',
                     'POS_SMILEY', 'NEG_SMILEY', 'NEGATION', 'REFERENCE', 'questionmark', 'exclamation', 'fullstop',
                     'polarity', 'RT', 'LAUGH', 'LOVE',  'postags', 'words', 'swn_score',  's_word', 't_similarity',
                     'res', 'lin', 'wup', 'path', 'contains_']

    def __init__(self, list_of_selected_features):
        self.selected_features = list_of_selected_features
        self.insert_q = '''INSERT INTO gistcloud_db.SelectedFeatures
        ( OH_SO, DONT_YOU, AS_GROUND_AS_VEHICLE, CAPITAL, HT, HT_POS, HT_NEG, LINK, POS_SMILEY,
        NEG_SMILEY, NEGATION, REFERENCE, questionmark, exclamation, fullstop, polarity, RT, LAUGH, LOVE, postags, words,
        swn_score, s_word, t_similarity, res, lin, wup, path, contains_)
        VALUES
        ("{OH_SO}", "{DONT_YOU}","{AS_GROUND_AS_VEHICLE}","{CAPITAL}","{HT}","{HT_POS}","{HT_NEG}","{LINK}",
        "{POS_SMILEY}","{NEG_SMILEY}","{NEGATION}","{REFERENCE}","{questionmark}","{exclamation}","{fullstop}",
        "{polarity}","{RT}","{LAUGH}", "{LAUGH}", "{postags}", "{words}", "{swn_score}", "{s_word}", "{t_similarity}",
        "{res}", "{lin}", "{wup}", "{path}", "{contains_}");
        '''
        # ("{0}", "{1}","{2}","{3}","{4}","{5}","{6}","{7}",
        #     "{8}","{9}","{10}","{11}","{12}","{13}","{14}",
        # "{15}","{16}","{17}", "{18}", "{19}", "{20}", "{21}", "{22}", "{23}");'''


    def save(self):
        q = self.insert_q
        a = '{'
        for each in self.selected_features_init:
            if each in self.selected_features:
                # v = exec("%s = %s" % (each, 1))
                a += "'" + each + '\':1, '
            else:
                a += "'" + each + '\':0, '

        a += "}"
        lastrow = g.mysql_conn.update(q.format(**ast.literal_eval(a)))
        return lastrow
