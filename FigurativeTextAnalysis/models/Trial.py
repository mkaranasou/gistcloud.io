from datetime import datetime
import nltk
from FigurativeTextAnalysis.models.Scorer import Scorer
from FigurativeTextAnalysis.models.SelectedFeatures import SelectedFeatures
from FigurativeTextAnalysis.processors.TweetProcessor import TweetProcessor
from FigurativeTextAnalysis.helpers.globals import g
from TrialScore import TrialScore

__author__ = 'maria'


class Trial(object):
    """

    """
    def __init__(self, selected_features, main_classifier, helper_classifier, discretization, test_or_final_ds="Final"):
        self.tweet_processor = TweetProcessor("", "", selected_features,
                                              main_classifier,
                                              helper_classifier,
                                              discretization, test_or_final_ds)
        self._id = None
        self.discretization = discretization
        self.selected_features = SelectedFeatures(selected_features)
        self.selected_features_id = None
        self.main_classifier = main_classifier
        self.helper_classifier = helper_classifier
        self.predictions = []
        self.x_train = []
        self.y_train = []
        self.x_test = []
        self.y_test = []
        self.trial_set_len = 0
        self.test_set_len = 0
        self.match = 0.0
        self.accuracy = 0.0
        self.precision = None
        self.recall = None
        self.f_measure = None
        self.cosine_similarity = None
        self.labels_range = None
        self.date_created = datetime.today()

    @staticmethod
    def insert_q():
            return '''INSERT INTO gistcloud_db.Trial
            ( selected_features_id, discretization, main_classifier, helper_classifier, trial_set_len, test_set_len, matched,
                correct, precision_, recall, f_measure, cosine_similarity, labels_range, date_created)
            VALUES
            ("{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}", "{7}", "{8}", "{9}", "{10}", "{11}", "{12}", "{13}");'''

    # 1. Process is either for pre-processing or for classification.
    def process(self):
        self.selected_features_id = self.selected_features.save()  # save selected features and keep id
        self.tweet_processor.get_train_set_from_db()
        self.tweet_processor.get_test_set_from_db()
        # tweet_processor.get_feature_trainset()
        # tweet_processor.get_feature_testset()
        # tweet_processor.reprocess()
        # tweet_processor.process_tweet_set(tweet_processor.train_set)
        # tweet_processor.process_tweet_set(tweet_processor.test_set)
        self.tweet_processor.classify()  # classify

    def save_results(self):
        """
        Gets metrics and data sets and saves Trial and Score results
        :return:
        :rtype:
        """
        self.predictions = self.tweet_processor.predictions
        self.x_train = self.tweet_processor.feature_list_train
        self.x_test = self.tweet_processor.feature_list_test
        self.y_train = self.tweet_processor.score_list_train
        self.y_test = self.tweet_processor.score_list_test
        self.trial_set_len = len(self.tweet_processor.train_set)
        self.test_set_len = len(self.tweet_processor.test_set)
        self.match = self.calculate_match()
        self.accuracy = nltk.metrics.accuracy(self.y_test, self.predictions)
        print "accuracy", self.accuracy
        print self.tweet_processor.classifier.get_most_informant_features(self.tweet_processor.classifier.cls,
                                                                          self.tweet_processor.classifier.vec)
        self.precision = nltk.metrics.precision(set(self.y_test), set(self.predictions))
        self.recall = nltk.metrics.recall(set(self.y_test), set(self.predictions))
        self.f_measure = nltk.metrics.f_measure(set(self.y_test), set(self.predictions))
        self.cosine_similarity = self.calculate_cosine_similarity(self.tweet_processor.test_set, self.predictions)
        self.labels_range = str(self.tweet_processor.discrete_labels).replace("'", "\\'")

        correct = float(self.match)/float(len(self.predictions)) if len(self.predictions) > 0 else 0.0
        q = self.insert_q().format(self.selected_features_id, self.discretization,
                                 g.CLASSIFIER_TYPE.reverse_mapping[self.main_classifier],
                                 g.CLASSIFIER_TYPE.reverse_mapping[self.helper_classifier],
                                 self.trial_set_len, self.test_set_len, self.accuracy,
                                 correct,
                                 self.precision, self.recall, self.f_measure, self.cosine_similarity,
                                 self.labels_range,
                                 self.date_created)
        lastrow = g.mysql_conn.update(q)
        self._id = lastrow

        # Save individual scores
        trial_score = TrialScore(self.tweet_processor.test_set, self.predictions)
        trial_score.store_scores_for_trial(self._id)

    def calculate_cosine_similarity(self, test_set, predictions):
        """
        Given the test_set and the corresponding predictions, cosine similarity is calculated using Scorer.
        :param test_set: The list of TweetBO's provided to the chosen classifier for prediction.
        :type test_set: list of TweetBO objects
        :param predictions: The classifier's results (correspond one by one with tweets in test_set)
        :type predictions: list of float numbers
        :return: total cosine similarity
        :rtype: float
        """
        gold_list = []
        predictions_list = []
        for tweet in test_set:
            gold_list.append([tweet.id, round(tweet.initial_score, 0)])
            if type(predictions[0]) == str:
                i = test_set.index(tweet)
                predictions_list.append([tweet.id, round(((self.tweet_processor.discrete_labels[predictions[i]][0] +
                                                    self.tweet_processor.discrete_labels[predictions[i]][1])/2.0), 0)])

            else:
                predictions_list.append([tweet.id, predictions[test_set.index(tweet)]])
        scorer = Scorer(gold_list, predictions_list)
        return scorer.final_score

    def calculate_match(self):
        """
        :return: Percentage of correct predictions
        :rtype: float
        """
        if len(self.y_test) > 0:
            return float(self.match) / float(len(self.y_test))
        return 0


# ============================================== TEST ================================================================ #
# # * : final combination of features
# selected_features =[
#                     'OH_SO',                  # * // <<   +
#                     'DONT_YOU',               # * // <<   +
#                     'AS_GROUND_AS_VEHICLE',   # * // <<   +
#                     'CAPITAL',                # * // <<   +
#                     'HT',                     # * <<      +
#                     'HT_POS',               # * // <<   +
#                     'HT_NEG',               # * // <<   +
#                     # # 'LINK',               # //
#                     'POS_SMILEY',           # * // << +
#                     'NEG_SMILEY',           # * // << +
#                     'NEGATION',             # * // << +
#                     'REFERENCE',            # * // << +
#                     'questionmark',         # * // << +
#                     'exclamation',          # * // << +
#                     # # 'fullstop',
#                     # # 'polarity',           #
#                     # # 'RT',                 # //
#                     # # 'LAUGH',              # //
#                     # # 'LOVE',               # //
#                     'postags',              # * // << +
#                     # # 'words',              #
#                     # # 'swn_score',
#                     's_word',               # * // << +
#                     # # 't_similarity',
#                     'res',                  # * <<
#                     # # 'lin',
#                     # # 'wup',
#                     # # 'path',               # //
#                     # 'contains_'
#                     ]
# trial = Trial(selected_features, g.CLASSIFIER_TYPE.SVMStandalone, g.CLASSIFIER_TYPE.DecisionTree, g.DISCRETIZATION.ONE)
# trial.process()
# trial.save_results()
