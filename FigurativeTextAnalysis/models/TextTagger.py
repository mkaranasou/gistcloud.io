__author__ = 'maria'

import re
import traceback
from HashTagHandler import HashTagHandler

__author__ = 'maria'
from FigurativeTextAnalysis.helpers.globals import g

"""
Todo:
1. Tag All corpuses and use them to train svm - ok
2. Keep a data part for test
3. When classifying new tweets:
    Clean them and tag them.
4. add positive HT and negative HT
"""


class TextTagger(object):
    """
    TextTagger is responsible for applying identified tags to a cleaned text.
    The result should be:
    {REFERENCE} {LINK}
    """
    _link_pattern = g.LINK_PATTERN
    _pos_smiley_pattern = g.POS_SMILEY_PATTERN
    _neg_smiley_pattern = g.NEG_SMILEY_PATTERN
    _negation_pattern = g.NEGATIONS_PATTERN
    _reference_pattern = g.REFERENCE_PATTERN
    _ht_pattern = g.HT_PATTERN
    _rt_pattern = g.RT_PATTERN
    _laugh_pattern = g.LAUGH_PATTERN
    _love_pattern = g.LOVE_PATTERN
    _capital_pattern = g.CAPITALS_PATTERN
    _oh_so_pattern = g.OH_SO_PATTERN
    _dont_you_pattern = g.DONT_YOU_PATTERN
    _as_ground_as_vehicle_pattern = g.AS_GROUND_AS_VEHICLE_PATTERN
    _patterns_ = {
        g.TAGS.CAPITAL: _capital_pattern,
        g.TAGS.HT: _ht_pattern,
        g.TAGS.LINK: _link_pattern,
        g.TAGS.POS_SMILEY: _pos_smiley_pattern,
        g.TAGS.NEG_SMILEY: _neg_smiley_pattern,
        g.TAGS.NEGATION: _negation_pattern,
        g.TAGS.REFERENCE: _reference_pattern,
        g.TAGS.RT: _rt_pattern,
        g.TAGS.LAUGH: _laugh_pattern,
        g.TAGS.LOVE: _love_pattern,
        g.TAGS.OH_SO: _oh_so_pattern,
        g.TAGS.DONT_YOU: _dont_you_pattern,
        g.TAGS.AS_GROUND_AS_VEHICLE: _as_ground_as_vehicle_pattern,
    }
    _tags = {
        g.TAGS.reverse_mapping[g.TAGS.CAPITAL]: "False",
        g.TAGS.reverse_mapping[g.TAGS.HT]: "False",
        g.TAGS.reverse_mapping[g.TAGS.HT_POS]: "False",
        g.TAGS.reverse_mapping[g.TAGS.HT_NEG]: "False",
        g.TAGS.reverse_mapping[g.TAGS.LINK]: "False",
        g.TAGS.reverse_mapping[g.TAGS.POS_SMILEY]: "False",
        g.TAGS.reverse_mapping[g.TAGS.NEG_SMILEY]: "False",
        g.TAGS.reverse_mapping[g.TAGS.NEGATION]: "False",
        g.TAGS.reverse_mapping[g.TAGS.REFERENCE]: "False",
        g.TAGS.reverse_mapping[g.TAGS.RT]: "False",
        g.TAGS.reverse_mapping[g.TAGS.LAUGH]: "False",
        g.TAGS.reverse_mapping[g.TAGS.LOVE]: "False",
        g.TAGS.reverse_mapping[g.TAGS.OH_SO]: "False",
        g.TAGS.reverse_mapping[g.TAGS.DONT_YOU]: "False",
        g.TAGS.reverse_mapping[g.TAGS.AS_GROUND_AS_VEHICLE]: "False"
    }

    def __init__(self):
        self.initial_text = ""
        self.tagged_text = ""
        self.tag_pattern = " {%s} "
        self.ht_handler = HashTagHandler()

    def tag_text(self, text):
        """
        replaces text_to_tag tag part with appropriate tag label
        e.g. http://... ==> {LINK}
        """
        self.ht_handler.reset()
        self.initial_text = text
        self.tagged_text = text
        self.reset()
        for key in self._patterns_:
            if key == g.TAGS.CAPITAL:
                match = re.findall(self._patterns_[key], self.initial_text)
                if match.__len__() > 0:
                    self.tagged_text += self.tag_pattern % self._tag(key)
                    self._tags[g.TAGS.reverse_mapping[key]] = "True"
            elif key == g.TAGS.HT:
                # find all hashtags
                ht = re.findall(self._patterns_[key], self.initial_text)
                if None != ht and [] != ht:                         # if found
                    ht_key = ''
                    if type(ht) == list:                            # if more than one
                        for each in ht:                             # for each in found hashtags
                            ht_key = self.ht_handler.handle(each)   # find if it is a positive or negative or neutral
                    else:                                           # not a list
                        ht_key = self.ht_handler.handle(ht)         # find if it is a positive or negative or neutral
                    self.tagged_text = re.sub(self._patterns_[key], self.tag_pattern % self._tag(key), self.tagged_text.lower())
                    self._tags[g.TAGS.reverse_mapping[ht_key]] = "True"   # replace the final verdict
                                                                          # of Hashtag handler with "True"
            elif key == g.TAGS.HT_NEG or key == g.TAGS.HT_POS:
                pass
            else:
                if re.findall(self._patterns_[key], self.initial_text):
                    self._tags[g.TAGS.reverse_mapping[key]] = "True"
                    self.tagged_text = re.sub(self._patterns_[key], self.tag_pattern % self._tag(key), self.tagged_text.lower())
                    if key == g.TAGS.LINK:
                        self.initial_text = re.sub(self._patterns_[key], "", self.initial_text)
                if re.findall(self._patterns_[key], self.initial_text.lower()):
                    self._tags[g.TAGS.reverse_mapping[key]] = "True"
                    self.tagged_text = re.sub(self._patterns_[key], self.tag_pattern % self._tag(key), self.tagged_text.lower())
                    if key == g.TAGS.LINK:
                        self.initial_text = re.sub(self._patterns_[key], "", self.initial_text)
        return self.tagged_text

    def identify_tags(self, text):
        pass

    def _tag(self, tag):
        return g.TAGS.reverse_mapping[tag]

    def reset(self):
        for key in self._tags.keys():
            self._tags[key] = "False"

