__author__ = 'maria'
import globals as g
'''
http://stackoverflow.com/questions/929103/convert-a-number-range-to-another-range-maintaining-ratio
NewValue = (((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin
Or a little more readable:

OldRange = (OldMax - OldMin)
NewRange = (NewMax - NewMin)
NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin
Or if you want to protect for the case where the old range is 0 (OldMin = OldMax):

OldRange = (OldMax - OldMin)
if (OldRange == 0)
    NewValue = NewMin
else
{
    NewRange = (NewMax - NewMin)
    NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin
}
Note that in this case we're forced to pick one of the possible new range values arbitrarily.
Depending on context, sensible choices could be: NewMin (see sample), NewMax or (NewMin + NewMax) / 2
'''

class ScaleConverter(object):
    def __init__(self, old_max=2.0, old_min=0.0, new_max=5.0, new_min=-5.0):
        self.old_max = old_max
        self.old_min = old_min
        self.new_max = new_max
        self.new_min = new_min
        self.old_range = (old_max - old_min)
        self.new_range = (new_max - new_min)

    def convert(self, old_value):
        if self.old_range == 0:
            return self.new_min
        return (((old_value - self.old_min)*self.new_range)/self.old_range) + self.new_min

# scale_c = ScaleConverter()
# print scale_c.convert(0.85)


# 0.85 --> 1.0
# x? --> 5.0  (((0.85 - 0) * 5) / 1.0) + -5
#
#
